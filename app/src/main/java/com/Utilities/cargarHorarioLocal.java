package com.Utilities;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import com.BaseDatosLocal.AdminSQLiteOpenHelper;
import com.BaseDatosLocal.HorariosLocal;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by varelald on 28/02/2018.
 */

public class cargarHorarioLocal {

    public static HorariosLocal[] mHorario;
    public static List<HorariosLocal> valuesHorarios = new ArrayList<HorariosLocal>();
    private static int COUNTt;
    public static String mLista;

    public static void cargarHorarioLocal (String idAlumno, Context pContext) {

        Context mContext = pContext;

        // Add some sample items.


        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper( mContext,
                "administracion_es", null, 1);

        SQLiteDatabase bd = admin.getWritableDatabase();

        Log.d("Alumno Carga Imagen",idAlumno);

        String sqlQuery =  "select h.horaInicio, h.horaFin, h.periodoMinutos from alumno a, horario h where h.idgrado =a.grado and  idAlum = " + idAlumno;

        Cursor fila = bd.rawQuery(sqlQuery, null);

        mHorario = new HorariosLocal[fila.getCount()];
        int i = 0;



        if (fila.moveToFirst()) {
            do {

               // Log.d("Fila:",fila.getString(0));

                String mHoraInicio, mMinutoInicio, mHoraFin,mMinutoFin, mPerioroMinutos;

                mHoraInicio =  fila.getString(0).substring(0,2);
                mMinutoInicio = fila.getString(0).substring(3);
                mHoraFin = fila.getString(1).substring(0,2);
                mMinutoFin = fila.getString(1).substring(3);
                mPerioroMinutos = fila.getString(2);

                mHorario[i] = new HorariosLocal(mHoraInicio,
                                         mMinutoInicio,
                                        mHoraFin,
                                        mMinutoFin,
                                        mPerioroMinutos);
                Log.d("Fila:", String.valueOf(mHorario.length));
                i = i + 1;
            } while (fila.moveToNext());
        } else{

            bd.close();

        }


        if ( mHorario == null||mHorario.length == 0){

            mHorario = new HorariosLocal[1];

            mHorario[0] = new HorariosLocal("00",
                    "00",
                    "00",
                    "00",
                    "1");
            return;
        }

        mLista = mHorario.toString();

        Log.d("Mensaje","Datos del JSON" + mHorario.toString());

        //ObtenerMetas.setContext(mcontext);
        //metas = ObtenerMetas.RecuperarMetas(mcontext);
//        ITEMS.clear();


        if (mHorario != null) {
            COUNTt = mHorario.length;
            for (int j = 0; j <= COUNTt - 1; j++) {

                valuesHorarios.add( mHorario[j]);


            }
            Log.d("HorariosLocal",valuesHorarios.toString());
        }



    }

}
