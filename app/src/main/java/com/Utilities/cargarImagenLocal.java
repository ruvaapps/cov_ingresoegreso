package com.Utilities;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import com.BaseDatosLocal.AdminSQLiteOpenHelper;
import com.BaseDatosLocal.Alumno;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by varelald on 28/02/2018.
 */

public class cargarImagenLocal {

    public static String[] mImagen;
    public static List<Bitmap> valuesImagenes;
    private static int COUNTt;
    public static String mLista;

    public static void cargarImagenLocal (String idAlumno, Context pContext) {

        Context mContext = pContext;

        // Add some sample items.


        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper( mContext,
                "administracion_es", null, 1);

        SQLiteDatabase bd = admin.getWritableDatabase();

        Log.d("Alumno Carga Imagen",idAlumno);

        String sqlQuery =  "select foto from alumno where idAlum = " + idAlumno;

        Cursor fila = bd.rawQuery(sqlQuery, null);

        mImagen = new String[fila.getCount()];
        int i = 0;



        if (fila.moveToFirst()) {
            do {

               // Log.d("Fila:",fila.getString(0));

                mImagen[i] = new String(fila.getString(0));
                Log.d("Fila:", String.valueOf(mImagen.length));
                i = i + 1;
            } while (fila.moveToNext());
        } else{

            bd.close();

        }


        if ( mImagen == null||mImagen.length == 0){return;}

        mLista = mImagen.toString();

        Log.d("Mensaje","Datos del JSON" + mImagen.toString());

        //ObtenerMetas.setContext(mcontext);
        //metas = ObtenerMetas.RecuperarMetas(mcontext);
//        ITEMS.clear();

        valuesImagenes = new ArrayList<Bitmap>();

        if (mImagen != null) {
            COUNTt = mImagen.length;
            Bitmap Imagen = null;
            for (int j = 0; j <= COUNTt - 1; j++) {
                try {
                    byte[] byteData = Base64.decode(mImagen[j], Base64.DEFAULT);
                    Imagen = BitmapFactory.decodeByteArray( byteData, 0,
                            byteData.length);
                }
                catch(Exception e) {
                    e.printStackTrace();
                }

                valuesImagenes.add( Imagen);


            }
            Log.d("Alumnos",valuesImagenes.toString());
        }



    }

}
