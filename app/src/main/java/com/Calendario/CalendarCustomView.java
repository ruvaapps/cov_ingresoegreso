package com.Calendario;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.BaseDatosLocal.Evento;
import com.covingresoegreso.MainActivity;
import com.covingresoegreso.R;

public class CalendarCustomView extends LinearLayout{
    private static final String TAG = CalendarCustomView.class.getSimpleName();
    private ImageView previousButton, nextButton;
    private TextView currentDate;
    public TextView mHora,mDom,mLun,mMat,mMie,mJue,mVie,mSab;
    private GridView calendarGridView;
    private Button addEventButton;
    private static int MAX_CALENDAR_COLUMN = 192;
    private int month, year;
    private SimpleDateFormat formatter = new SimpleDateFormat("dd", Locale.ENGLISH);
    private SimpleDateFormat formatterMes = new SimpleDateFormat("MMMM", Locale.ENGLISH);
    private SimpleDateFormat formatterAnio = new SimpleDateFormat("yyyy", Locale.ENGLISH);
    private SimpleDateFormat formatterMiMes = new SimpleDateFormat("MM", Locale.ENGLISH);
    private Calendar cal = Calendar.getInstance(Locale.ENGLISH);
    private Context context;
    private GridAdapter mAdapter;
    private DatabaseQuery mQuery;
    private static int g;
    private static int diaPrimero, diaUltimo;
    private static int mHoraInicio,mMinutoInicio,mMinutoPeriodo,mCantidadPeriodos,mHoraFin,mMinutoFin;
    public static Evento[] mEvents;
    private String hrCelda="";
    private int hg;
    private Calendar hsCal;
    private String miMes;

    public CalendarCustomView(Context context) {
        super(context);

    }
    public CalendarCustomView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;

        mHoraInicio = MainActivity.mHoraInicio;
        mMinutoInicio = MainActivity.mMinutoInicio;

        mMinutoPeriodo = MainActivity.mMinutoPeriodo;


        mHoraFin = MainActivity.mHoraFin;
        mMinutoFin = MainActivity.mMinutoFin;

        initializeUILayout();
        setUpCalendarAdapter();
        //setPreviousButtonClickEvent();
        //setNextButtonClickEvent();
        setGridCellClickEvents();
        Log.d(TAG, "I need to call this method");


    }
    public CalendarCustomView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    private void initializeUILayout(){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.calendar_layout, this);
        //previousButton = (ImageView)view.findViewById(R.id.previous_month);
        //nextButton = (ImageView)view.findViewById(R.id.next_month);
        //currentDate = (TextView)view.findViewById(R.id.display_current_date);


        //addEventButton = (Button)view.findViewById(R.id.add_calendar_event);
        //addEventButton.setVisibility(INVISIBLE);
        calendarGridView = (GridView)view.findViewById(R.id.calendar_grid);
        mHora = (TextView)view.findViewById(R.id.horas);
        mDom = (TextView)view.findViewById(R.id.sun);
        mLun = (TextView)view.findViewById(R.id.mon);
        mMat = (TextView)view.findViewById(R.id.tue);
        mMie = (TextView)view.findViewById(R.id.wed);
        mJue = (TextView)view.findViewById(R.id.thu);
        mVie = (TextView)view.findViewById(R.id.fri);
        mSab = (TextView)view.findViewById(R.id.sat);

    }

/*
    private void setPreviousButtonClickEvent(){
        previousButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                //cal.add(Calendar.MONTH, -1);
                cal.add(Calendar.DAY_OF_WEEK, -7);
                setUpCalendarAdapter();


            }
        });
    }
    private void setNextButtonClickEvent(){
        nextButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                //cal.add(Calendar.MONTH, 1);
                cal.add(Calendar.DAY_OF_WEEK, 7);
                setUpCalendarAdapter();
            }
        });
    }
    */

    private void setGridCellClickEvents(){
        calendarGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                MainActivity.setearPager(1);
                Toast.makeText(context, "Se llamara a la funcion para grabar el evento "+ parent.getItemAtPosition(position).toString(), Toast.LENGTH_LONG).show();

                //Toast.makeText(context, "Clicked " + parent.getItemAtPosition(position).toString(), Toast.LENGTH_LONG).show();




            }
        });
    }
    private void setUpCalendarAdapter(){
        List<Date> dayValueInCells = new ArrayList<Date>();

        List<String> stringValueInCells = new ArrayList<String>();

        /*mQuery = new DatabaseQuery(context);
        List<EventObjects> mEvents = mQuery.getAllFutureEvents();*/

        mEvents = MainActivity.mEven;

        Calendar mCalHoy = (Calendar)cal.clone();

        Calendar mCal = (Calendar)cal.clone();

        Calendar mCal1 = (Calendar)cal.clone();

        Calendar mCal2 = (Calendar)cal.clone();

        mCal1.set(Calendar.DAY_OF_MONTH, 1);

        mCal.set(Calendar.HOUR_OF_DAY, 1);

        int firstDayOfTheMonth1 = mCal1.get(Calendar.DAY_OF_WEEK);
        int firstDayOfTheMonth = mCal.get(Calendar.HOUR_OF_DAY) - 1;

        Log.d(TAG, "First Date" + firstDayOfTheMonth1);

        mCal1.add(Calendar.DAY_OF_MONTH, -firstDayOfTheMonth1);

        mCal.add(Calendar.HOUR_OF_DAY, 0);

        /**********Este es el calculo de celdas que se van a generar***********/
        Calendar calInicio = Calendar.getInstance();

        /*Hora inicio*/
        calInicio.set(Calendar.HOUR_OF_DAY, mHoraInicio);
        calInicio.set(Calendar.MINUTE, mMinutoInicio);
        calInicio.set(Calendar.SECOND, 00);

        Calendar calfin = Calendar.getInstance();
        /*Hora Fin*/
        calfin.set(Calendar.HOUR_OF_DAY, mHoraFin);
        calfin.set(Calendar.MINUTE, mMinutoFin);
        calfin.set(Calendar.SECOND, 00);

        int dif = calfin.get(Calendar.HOUR_OF_DAY)-calInicio.get(Calendar.HOUR_OF_DAY);
        int minInicio = calInicio.get(Calendar.MINUTE);
        int minFin = calfin.get(Calendar.MINUTE);
        int difMinutos = ((dif*60)-minInicio)+minFin;

        /*Periodo*/
        int cantColumnas = (difMinutos / mMinutoPeriodo);
        MAX_CALENDAR_COLUMN = cantColumnas *8;


        hsCal = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("HH:mm");



        /**********Fin del calculo de celdas que se van a generar***********/

        Log.d("Son los minutos: ","min: " + difMinutos);

        String FechaHoy = formatter.format(cal.getTime());
        String mesCorriente = formatterMes.format(cal.getTime());
        //miMes = formatterMiMes.format(cal.getTime());
        String miAnio = formatterAnio.format(cal.getTime());

        int diaHoy = Integer.parseInt(FechaHoy);

        int  sDate =       cal.get(Calendar.DAY_OF_WEEK);
        int sMes = cal.get(Calendar.MONTH);

        int sHoy = Integer.parseInt(formatter.format(mCalHoy.getTime()));
        String Mes,primerDia,ultimoDia;

        int svc = 0;
        while(dayValueInCells.size() < MAX_CALENDAR_COLUMN){

                dayValueInCells.add(mCal.getTime());
                stringValueInCells.add(""+svc);

                svc =svc+1;

        }


        /*
          if (position == 0){
            //Log.d("","posicion : "+position);
            hrCelda = String.valueOf(hrComienzo);
            cellNumber.setText(String.valueOf(hrComienzo));
            //mImagen.setVisibility(View.INVISIBLE);
        }else if (position%8 == 0){
           // Log.d("","posicion M : "+position);
            int periodo = mMinPeriodo*(position/8);
            hsCal.add(Calendar.MINUTE,periodo);
            String hrModulo = dateFormat.format(hsCal.getTime());
            cellNumber.setText(String.valueOf(hrModulo));
            hrCelda = hrModulo;

            //mImagen.setVisibility(View.INVISIBLE);
        }
        * */


        String Dia;
        switch (sDate) {
            case 1:
                mCal2.add(Calendar.DAY_OF_MONTH,0);
                String diaDomingo = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());


                mDom.setText("Dom \n"+ (Integer.parseInt(formatter.format(mCal2.getTime()))));
                diaPrimero = Integer.parseInt(formatter.format(mCal2.getTime()));
                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Domingo","pos: " + g);


                          if (g == 1){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaDomingo);
                    }else if ((g-1)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaDomingo);

                    }

                    g= g+1;
                }
                mCal2.add(Calendar.DAY_OF_MONTH,1);
                String diaLunes = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());


                mLun.setText("Lun \n"+ (Integer.parseInt(formatter.format(mCal2.getTime()))));
                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Domingo","pos: " + g);



                    if (g == 2){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaLunes);
                    }else if ((g-2)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaLunes);
                    }

                    g= g+1;
                }
                mCal2.add(Calendar.DAY_OF_MONTH,1);
                String diaMartes = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());

                mMat.setText("Mar \n"+ (Integer.parseInt(formatter.format(mCal2.getTime()))));
                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Domingo","pos: " + g);



                    if (g == 3){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaMartes);
                    }else if ((g-3)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaMartes);
                    }

                    g= g+1;
                }
                mCal2.add(Calendar.DAY_OF_MONTH,1);
                String diaMiercoles = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());

                mMie.setText("Mie \n"+ (Integer.parseInt(formatter.format(mCal2.getTime()))));
                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Domingo","pos: " + g);



                    if (g == 4){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaMiercoles);
                    }else if ((g-4)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaMiercoles);
                    }

                    g= g+1;
                }
                mCal2.add(Calendar.DAY_OF_MONTH,1);
                String diaJueves = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());

                mJue.setText("Jue \n"+ Integer.parseInt(formatter.format(mCal2.getTime())));
                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Domingo","pos: " + g);



                    if (g == 5){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaJueves);
                    }else if ((g-5)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaJueves);
                    }

                    g= g+1;
                }
                mCal2.add(Calendar.DAY_OF_MONTH,1);
                String diaViernes = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());

                mVie.setText("Vie \n"+ (Integer.parseInt(formatter.format(mCal2.getTime()))));
                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Domingo","pos: " + g);



                    if (g == 6){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaViernes);
                    }else if ((g-6)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaViernes);
                    }

                    g= g+1;
                }
                mCal2.add(Calendar.DAY_OF_MONTH,1);
                String diaSabado = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());

                mSab.setText("Sab \n"+ (Integer.parseInt(formatter.format(mCal2.getTime()))));
                diaUltimo = Integer.parseInt(formatter.format(mCal2.getTime()));
                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Domingo","pos: " + g);



                    if (g == 7){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaSabado);
                    }else if ((g-7)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaSabado);
                    }

                    g= g+1;
                }


                Dia = "Domingo";
                break;
            case 2:
                mCal2.add(Calendar.DAY_OF_MONTH,-1);
                diaDomingo = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());


                mDom.setText("Dom \n"+ (Integer.parseInt(formatter.format(mCal2.getTime()))));
                diaPrimero=Integer.parseInt(formatter.format(mCal2.getTime()));
                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Domingo","pos: " + g);

                    if (g == 1){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaDomingo);
                    }else if ((g-1)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaDomingo);
                    }

                    g= g+1;
                }
                mCal2.add(Calendar.DAY_OF_MONTH,1);
                diaLunes = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());

                mLun.setText("Lun \n"+ (Integer.parseInt(formatter.format(mCal2.getTime()))));
                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Domingo","pos: " + g);



                    if (g == 2){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaLunes);
                    }else if ((g-2)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaLunes);
                    }

                    g= g+1;
                }
                mCal2.add(Calendar.DAY_OF_MONTH,1);
                diaMartes = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());

                mMat.setText("Mar \n"+ (Integer.parseInt(formatter.format(mCal2.getTime()))));
                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Domingo","pos: " + g);



                    if (g == 3){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaMartes);
                    }else if ((g-3)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaMartes);
                    }

                    g= g+1;
                }
                mCal2.add(Calendar.DAY_OF_MONTH,1);
                diaMiercoles = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());

                mMie.setText("Mie \n"+ (Integer.parseInt(formatter.format(mCal2.getTime()))));
                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Domingo","pos: " + g);



                    if (g == 4){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaMiercoles);
                    }else if ((g-4)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaMiercoles);
                    }

                    g= g+1;
                }
                mCal2.add(Calendar.DAY_OF_MONTH,1);
                diaJueves = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());

                mJue.setText("Jue \n"+ Integer.parseInt(formatter.format(mCal2.getTime())));
                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Domingo","pos: " + g);



                    if (g == 5){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaJueves);
                    }else if ((g-5)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaJueves);
                    }

                    g= g+1;
                }
                mCal2.add(Calendar.DAY_OF_MONTH,1);
                diaViernes = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());

                mVie.setText("Vie \n"+ (Integer.parseInt(formatter.format(mCal2.getTime()))));
                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Domingo","pos: " + g);



                    if (g == 6){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaViernes);
                    }else if ((g-6)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaViernes);
                    }

                    g= g+1;
                }
                mCal2.add(Calendar.DAY_OF_MONTH,1);
                diaSabado = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());

                mSab.setText("Sab \n"+ (Integer.parseInt(formatter.format(mCal2.getTime()))));
                diaUltimo = Integer.parseInt(formatter.format(mCal2.getTime()));
                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Domingo","pos: " + g);



                    if (g == 7){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaSabado);
                    }else if ((g-7)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaSabado);
                    }

                    g= g+1;
                }

                Dia = "Lunes";
                break;
            case 3:
                mCal2.add(Calendar.DAY_OF_MONTH,-2);
                diaDomingo = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());

                mDom.setText("Dom \n"+ (Integer.parseInt(formatter.format(mCal2.getTime()))));
                diaPrimero = Integer.parseInt(formatter.format(mCal2.getTime()));
                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Domingo","pos: " + g);



                          if (g == 1){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaDomingo);
                    }else if ((g-1)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaDomingo);
                    }

                    g= g+1;
                }
                mCal2.add(Calendar.DAY_OF_MONTH,1);
                diaLunes = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());

                mLun.setText("Lun \n"+ (Integer.parseInt(formatter.format(mCal2.getTime()))));
                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Domingo","pos: " + g);



                    if (g == 2){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaLunes);
                    }else if ((g-2)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaLunes);
                    }

                    g= g+1;
                }
                mCal2.add(Calendar.DAY_OF_MONTH,1);
                diaMartes = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());

                mMat.setText("Mar \n"+ (Integer.parseInt(formatter.format(mCal2.getTime()))));
                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Domingo","pos: " + g);



                    if (g == 3){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaMartes);
                    }else if ((g-3)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaMartes);
                    }

                    g= g+1;
                }
                mCal2.add(Calendar.DAY_OF_MONTH,1);
                diaMiercoles = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());

                mMie.setText("Mie \n"+ (Integer.parseInt(formatter.format(mCal2.getTime()))));
                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Domingo","pos: " + g);



                    if (g == 4){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaMiercoles);
                    }else if ((g-4)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaMiercoles);
                    }

                    g= g+1;
                }
                mCal2.add(Calendar.DAY_OF_MONTH,1);
                diaJueves = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());

                mJue.setText("Jue \n"+ Integer.parseInt(formatter.format(mCal2.getTime())));
                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Domingo","pos: " + g);



                    if (g == 5){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaJueves);
                    }else if ((g-5)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaJueves);
                    }

                    g= g+1;
                }
                mCal2.add(Calendar.DAY_OF_MONTH,1);
                diaViernes = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());

                mVie.setText("Vie \n"+ (Integer.parseInt(formatter.format(mCal2.getTime()))));
                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Domingo","pos: " + g);



                    if (g == 6){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaViernes);
                    }else if ((g-6)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaViernes);
                    }

                    g= g+1;
                }
                mCal2.add(Calendar.DAY_OF_MONTH,1);
                diaSabado = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());

                mSab.setText("Sab \n"+ (Integer.parseInt(formatter.format(mCal2.getTime()))));
                diaUltimo = Integer.parseInt(formatter.format(mCal2.getTime()));
                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Domingo","pos: " + g);



                    if (g == 7){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaSabado);
                    }else if ((g-7)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaSabado);
                    }

                    g= g+1;
                }

                Dia = "Martes";
                break;
            case 4:
                mCal2.add(Calendar.DAY_OF_MONTH,-3);
                diaDomingo = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());

                mDom.setText("Dom \n"+ (Integer.parseInt(formatter.format(mCal2.getTime()))));
                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Domingo","pos: " + g);



                          if (g == 1){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaDomingo);
                    }else if ((g-1)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaDomingo);
                    }

                    g= g+1;
                }
                mCal2.add(Calendar.DAY_OF_MONTH,1);
                diaLunes = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());

                mLun.setText("Lun \n"+ (Integer.parseInt(formatter.format(mCal2.getTime()))));
                g=0;hg=0;
                while( g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Domingo","pos: " + g);



                    if (g == 2){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaLunes);
                    }else if ((g-2)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaLunes);
                    }

                    g= g+1;
                }
                mCal2.add(Calendar.DAY_OF_MONTH,1);
                diaMartes = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());

                mMat.setText("Mar \n"+ (Integer.parseInt(formatter.format(mCal2.getTime()))));
                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Domingo","pos: " + g);



                    if (g == 3){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaMartes);
                    }else if ((g-3)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaMartes);
                    }

                    g= g+1;
                }
                mCal2.add(Calendar.DAY_OF_MONTH,1);
                diaMiercoles = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());

                mMie.setText("Mie \n"+ (Integer.parseInt(formatter.format(mCal2.getTime()))));
                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Domingo","pos: " + g);



                    if (g == 4){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaMiercoles);
                    }else if ((g-4)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaMiercoles);
                    }

                    g= g+1;
                }
                mCal2.add(Calendar.DAY_OF_MONTH,1);
                diaJueves = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());

                mJue.setText("Jue \n"+ Integer.parseInt(formatter.format(mCal2.getTime())));
                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Domingo","pos: " + g);



                    if (g == 5){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaJueves);
                    }else if ((g-5)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaJueves);
                    }

                    g= g+1;
                }
                mCal2.add(Calendar.DAY_OF_MONTH,1);
                diaViernes = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());

                mVie.setText("Vie \n"+ (Integer.parseInt(formatter.format(mCal2.getTime()))));
                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Domingo","pos: " + g);



                    if (g == 6){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaViernes);
                    }else if ((g-6)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaViernes);
                    }

                    g= g+1;
                }
                mCal2.add(Calendar.DAY_OF_MONTH,1);
                diaSabado = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());

                mSab.setText("Sab \n"+ (Integer.parseInt(formatter.format(mCal2.getTime()))));
                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Domingo","pos: " + g);



                    if (g == 7){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaSabado);
                    }else if ((g-7)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaSabado);
                    }

                    g= g+1;
                }

                Dia = "Miercoles";
                break;
            case 5:

                mCal2.add(Calendar.DAY_OF_MONTH,-4);
                diaDomingo = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());

                mDom.setText("Dom \n"+ (Integer.parseInt(formatter.format(mCal2.getTime()))));

                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Domingo","pos: " + g);



                          if (g == 1){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaDomingo);
                    }else if ((g-1)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaDomingo);
                    }

                    g= g+1;
                }

                mCal2.add(Calendar.DAY_OF_MONTH,1);
                diaLunes = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());

                mLun.setText("Lun \n"+ (Integer.parseInt(formatter.format(mCal2.getTime()))));

                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Lunes","pos: " + g);



                    if (g == 2){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaLunes);
                    }else if ((g-2)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaLunes);
                    }

                    g= g+1;
                }
                mCal2.add(Calendar.DAY_OF_MONTH,1);
                diaMartes = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());

                mMat.setText("Mar \n"+ (Integer.parseInt(formatter.format(mCal2.getTime()))));
                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Martes","pos: " + g);



                    if (g == 3){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaMartes);
                    }else if ((g-3)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaMartes);
                    }

                    g= g+1;
                }
                mCal2.add(Calendar.DAY_OF_MONTH,1);
                diaMiercoles = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());

                mMie.setText("Mie \n"+ (Integer.parseInt(formatter.format(mCal2.getTime()))));

                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Miercoles","pos: " + g);



                    if (g == 4){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaMiercoles);
                    }else if ((g-4)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaMiercoles);
                    }

                    g= g+1;
                }
                mCal2.add(Calendar.DAY_OF_MONTH,1);
                diaJueves = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());

                mJue.setText("Jue \n"+ Integer.parseInt(formatter.format(mCal2.getTime())));

                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Jueves","pos: " + g);



                    if (g == 5){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaJueves);
                    }else if ((g-5)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaJueves);
                    }

                    g= g+1;
                }

                mCal2.add(Calendar.DAY_OF_MONTH,1);
                diaViernes = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());

                mVie.setText("Vie \n"+ (Integer.parseInt(formatter.format(mCal2.getTime()))));

                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Viernes","pos: " + g);



                    if (g == 6){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaViernes);
                    }else if ((g-6)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaViernes);
                    }

                    g= g+1;
                }
                mCal2.add(Calendar.DAY_OF_MONTH,1);
                diaSabado = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());

                mSab.setText("Sab \n"+ (Integer.parseInt(formatter.format(mCal2.getTime()))));

                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Sabado","pos: " + g);



                    if (g == 7){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaSabado);
                    }else if ((g-7)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaSabado);
                    }

                    g= g+1;
                }
                Dia = "Jueves";


                break;
            case 6:

                mCal2.add(Calendar.DAY_OF_MONTH,-5);
                diaDomingo = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());

                mDom.setText("Dom \n"+ (Integer.parseInt(formatter.format(mCal2.getTime()))));
                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Domingo","pos: " + g);



                          if (g == 1){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaDomingo);
                    }else if ((g-1)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaDomingo);
                    }

                    g= g+1;
                }

                mCal2.add(Calendar.DAY_OF_MONTH,1);
                diaLunes = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());

                mLun.setText("Lun \n"+ (Integer.parseInt(formatter.format(mCal2.getTime()))));
                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Lunes","pos: " + g);



                    if (g == 2){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaLunes);
                    }else if ((g-2)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaLunes);
                    }

                    g= g+1;
                }

                mCal2.add(Calendar.DAY_OF_MONTH,1);
                diaMartes = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());

                mMat.setText("Mar \n"+ (Integer.parseInt(formatter.format(mCal2.getTime()))));
                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion MArtes","pos: " + g);



                    if (g == 3){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaMartes);
                    }else if ((g-3)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaMartes);
                    }

                    g= g+1;
                }

                mCal2.add(Calendar.DAY_OF_MONTH,1);
                diaMiercoles = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());

                mMie.setText("Mie \n"+ (Integer.parseInt(formatter.format(mCal2.getTime()))));
                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Miercoles","pos: " + g);



                    if (g == 4){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaMiercoles);
                    }else if ((g-4)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaMiercoles);
                    }

                    g= g+1;
                }

                mCal2.add(Calendar.DAY_OF_MONTH,1);
                diaJueves = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());

                mJue.setText("Jue \n"+ Integer.parseInt(formatter.format(mCal2.getTime())));
                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Sabado","pos: " + g);



                    if (g == 5){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaJueves);
                    }else if ((g-5)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaJueves);
                    }

                    g= g+1;
                }

                mCal2.add(Calendar.DAY_OF_MONTH,1);
                diaViernes = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());

                mVie.setText("Vie \n"+ (Integer.parseInt(formatter.format(mCal2.getTime()))));
                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Sabado","pos: " + g);



                    if (g == 6){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaViernes);
                    }else if ((g-6)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaViernes);
                    }

                    g= g+1;
                }

                mCal2.add(Calendar.DAY_OF_MONTH,1);
                diaSabado = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());

                mSab.setText("Sab \n"+ (Integer.parseInt(formatter.format(mCal2.getTime()))));
                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Sabado","pos: " + g);



                    if (g == 7){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaSabado);
                    }else if ((g-7)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaSabado);
                    }

                    g= g+1;
                }

                Dia = "Viernes";
                break;
            case 7:
                mCal2.add(Calendar.DAY_OF_MONTH,-6);
                diaDomingo = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());

                mDom.setText("Dom \n"+ (Integer.parseInt(formatter.format(mCal2.getTime()))));
                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Domingo","pos: " + g);



                          if (g == 1){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaDomingo);
                    }else if ((g-1)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaDomingo);
                    }

                    g= g+1;
                }
                mCal2.add(Calendar.DAY_OF_MONTH,1);
                diaLunes = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());

                mLun.setText("Lun \n"+ (Integer.parseInt(formatter.format(mCal2.getTime()))));
                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Lunes","pos: " + g);



                    if (g == 2){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaLunes);
                    }else if ((g-2)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaLunes);
                    }

                    g= g+1;
                }
                mCal2.add(Calendar.DAY_OF_MONTH,1);
                diaMartes = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());

                mMat.setText("Mar \n"+ (Integer.parseInt(formatter.format(mCal2.getTime()))));
                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Martes","pos: " + g);



                    if (g == 3){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaMartes);
                    }else if ((g-3)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaMartes);
                    }

                    g= g+1;
                }
                mCal2.add(Calendar.DAY_OF_MONTH,1);
                diaMiercoles = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());

                mMie.setText("Mie \n"+ (Integer.parseInt(formatter.format(mCal2.getTime()))));
                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Miercoles","pos: " + g);



                    if (g == 4){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaMiercoles);
                    }else if ((g-4)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaMiercoles);
                    }

                    g= g+1;
                }
                mCal2.add(Calendar.DAY_OF_MONTH,1);
                diaJueves = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());

                mJue.setText("Jue \n"+ Integer.parseInt(formatter.format(mCal2.getTime())));
                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Jueves","pos: " + g);



                    if (g == 5){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaJueves);
                    }else if ((g-5)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaJueves);
                    }

                    g= g+1;
                }
                mCal2.add(Calendar.DAY_OF_MONTH,1);
                diaViernes = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());

                mVie.setText("Vie \n"+ (Integer.parseInt(formatter.format(mCal2.getTime()))));
                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Viernes","pos: " + g);



                    if (g == 6){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaViernes);
                    }else if ((g-6)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaViernes);
                    }

                    g= g+1;
                }
                mCal2.add(Calendar.DAY_OF_MONTH,1);
                diaSabado = String.valueOf((Integer.parseInt(formatter.format(mCal2.getTime()))));
                miMes = formatterMiMes.format(mCal2.getTime());

                mSab.setText("Sab \n"+ (Integer.parseInt(formatter.format(mCal2.getTime()))));
                g=0;hg=0;
                while(g < MAX_CALENDAR_COLUMN){
                    //Log.d("Posicion Sabado","pos: " + g);



                    if (g == 7){
                        hsCal.set(Calendar.HOUR_OF_DAY, mHoraInicio);
                        hsCal.set(Calendar.MINUTE, mMinutoInicio);
                        hsCal.set(Calendar.SECOND, 00);
                        hrCelda = dateFormat.format(hsCal.getTime());
                        hg = hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaSabado);
                    }else if ((g-7)%8 == 0){
                        int periodo = mMinutoPeriodo;
                        hsCal.add(Calendar.MINUTE,periodo);
                        String hrModulo = dateFormat.format(hsCal.getTime());
                        hrCelda = hrModulo;
                        hg=hg+1;

                        dayValueInCells.set(g,mCal2.getTime());
                        stringValueInCells.set(g,hrCelda+"¬"+miAnio+"/"+miMes+"/"+diaSabado);
                    }

                    g= g+1;
                }
                Dia = "Sabado";
                break;
            default:
                Dia = "";
                break;

        }




        //currentDate.setText("Semana \nDel "+ diaPrimero +" al "+ diaUltimo + " de "+mesCorriente);






        //mAdapter = new GridAdapter(context, dayValueInCells, cal, mEvents,mHoraInicio,mMinutoInicio,mMinutoPeriodo);
        mAdapter = new GridAdapter(context, stringValueInCells, dayValueInCells, cal, mEvents,mHoraInicio,mMinutoInicio,mMinutoPeriodo);

        calendarGridView.setAdapter(mAdapter);
    }
}