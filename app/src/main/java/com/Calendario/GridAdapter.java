package com.Calendario;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;



import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.BaseDatosLocal.Evento;
import com.covingresoegreso.R;

public class GridAdapter extends ArrayAdapter {
    private static final String TAG = GridAdapter.class.getSimpleName();
    private LayoutInflater mInflater;
    private List<Date> monthlyDates;
    private List<String>stringList;
    private Calendar currentDate;
    private int mHoraIniocio,mMinutoInicio,mMinPeriodo,mHoraFin,mMinutoFin;
    private Evento[] allEvents;
    private static String hrCelda= "0";

    public GridAdapter(Context context,List<String>stringList ,List<Date> monthlyDates, Calendar currentDate, Evento[] allEvents, int horaInicio, int minutoInicio, int minPeriodo) {
        super(context, R.layout.single_cell_layout);
        this.monthlyDates = monthlyDates;
        this.stringList = stringList;
        this.currentDate = currentDate;
        this.allEvents = allEvents;
        this.mHoraIniocio = horaInicio;
        this.mMinutoInicio = minutoInicio;
        this.mMinPeriodo = minPeriodo;
        mInflater = LayoutInflater.from(context);
    }
    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Date mDate = monthlyDates.get(position);

        Calendar dateCal = Calendar.getInstance();

        Calendar hsCal = Calendar.getInstance();


        dateCal.setTime(mDate);

        int dayWeek = dateCal.get(Calendar.DAY_OF_WEEK);
        int dayValue = dateCal.get(Calendar.DAY_OF_MONTH);
        int displayMonth = dateCal.get(Calendar.MONTH) + 1;
        int displayYear = dateCal.get(Calendar.YEAR);
        int currentMonth = currentDate.get(Calendar.MONTH) + 1;
        int currentYear = currentDate.get(Calendar.YEAR);

        DateFormat dateFormat = new SimpleDateFormat("HH:mm");
        hsCal.set(Calendar.HOUR_OF_DAY, mHoraIniocio);
        hsCal.set(Calendar.MINUTE, mMinutoInicio);
        hsCal.set(Calendar.SECOND, 00);

        String hrComienzo = dateFormat.format(hsCal.getTime());



        View view;// = convertView;
       /* if(view == null){
            view = mInflater.inflate(R.layout.single_cell_layout, parent, false);
        }*/

        view = mInflater.inflate(R.layout.single_cell_layout, parent, false);

        /*if(displayMonth == currentMonth && displayYear == currentYear){
            view.setBackgroundColor(Color.parseColor("#FF5733"));
        }else{
            view.setBackgroundColor(Color.parseColor("#cccccc"));
        }*/

        //Add day to calendar
        TextView cellNumber = (TextView)view.findViewById(R.id.calendar_date_id);





        ImageView mImagen = (ImageView) view.findViewById(R.id.calendar_imagen);

        /*voy a determinar si es multiplo de 8, la celda que sea multiplo de 8
        * sera la celda en la que va la hora de esa fila, para determinar la hora que debo poner
        * vamos a poner en la celda CERO, la hora de inicio del calendario, luego vemos la multiplicidad de la celda
        * y vamos a multiplicar el periodo definido para el calendario, por la multiplicidad de la celda, lo que
        * nos dara el horario para esa fila*/


        //String hrCelda = "0";

        if (position == 0){
            //Log.d("","posicion : "+position);
            hrCelda = String.valueOf(hrComienzo);
            cellNumber.setText(String.valueOf(hrComienzo));
            //mImagen.setVisibility(View.INVISIBLE);
        }else if (position%8 == 0){
           // Log.d("","posicion M : "+position);
            int periodo = mMinPeriodo*(position/8);
            hsCal.add(Calendar.MINUTE,periodo);
            String hrModulo = dateFormat.format(hsCal.getTime());
            cellNumber.setText(String.valueOf(hrModulo));
            hrCelda = hrModulo;

            //mImagen.setVisibility(View.INVISIBLE);
        }else{

        //    cellNumber.setText(String.valueOf(displayMonth)+"¬"+String.valueOf(dayValue)+"¬"+hrCelda);


            for(int i = 0; i < allEvents.length; i++) {


                if (hrCelda.equals(allEvents[i].getHora()) && String.valueOf(dayValue).equals(allEvents[i].getDia()) && String.valueOf(displayMonth).equals(allEvents[i].getMes())&& String.valueOf(currentYear).equals(allEvents[i].getAnio()) ) {

                    //mImagen.setImageResource(R.mipmap.mamadera);


                    String miImagen = allEvents[i].getImagen();

                    int res = getContext().getResources().getIdentifier(miImagen, "mipmap", getContext().getPackageName());
                    mImagen.setImageResource(res);

                    //imageview= (ImageView)findViewById(R.id.imageView);
                    //imageview.setImageResource(res);


                    Log.d("Renderizar", "Mamadera");


                } else {
                    mImagen.setImageResource(R.mipmap.nube_vacia);
                }

            }
        }

        // Log.d("Hora de la celda",position+"-"+ hrCelda +"-"+dayValue);




        //Add events to the calendar
        /*TextView eventIndicator = (TextView)view.findViewById(R.id.event_id);

        Calendar eventCalendar = Calendar.getInstance();
        for(int i = 0; i < allEvents.size(); i++){

            eventCalendar.setTime(allEvents.get(i).getDate());
            if(dayValue == eventCalendar.get(Calendar.DAY_OF_MONTH) && displayMonth == eventCalendar.get(Calendar.MONTH) + 1
                    && displayYear == eventCalendar.get(Calendar.YEAR)){
                eventIndicator.setBackgroundColor(Color.parseColor("#FF4081"));
            }
        }*/

        return view;
    }
    @Override
    public int getCount() {
        return stringList.size();
    }
    @Nullable
    @Override
    public Object getItem(int position) {
        return stringList.get(position);
    }
    @Override
    public int getPosition(Object item) {
        return stringList.indexOf(item);
    }


}