package com.Calendario;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.BaseDatosLocal.AdminSQLiteOpenHelper;

public class DatabaseObject {
    private static AdminSQLiteOpenHelper dbHelper;
    private SQLiteDatabase db;
    public DatabaseObject(Context context) {
        dbHelper = new AdminSQLiteOpenHelper(context,
                "administracion_es", null, 1);
        this.dbHelper.getWritableDatabase();
        this.db = dbHelper.getReadableDatabase();
    }
    public SQLiteDatabase getDbConnection(){
        return this.db;
    }
    public void closeDbConnection(){
        if(this.db != null){
            this.db.close();
        }
    }
}
