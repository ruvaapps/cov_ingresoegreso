package com.JSONs;

import android.app.ProgressDialog;
import android.util.Log;

import com.BaseDatosLocal.Alumno;
import com.BaseDatosLocal.Horarios;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by varelald on 08/03/2018.
 */

public class getJsonHorarios {

    public static String mjson;
    public static Horarios[] horarios;
    private static Gson gson = new Gson();
    public static int Progreso;
    private ProgressDialog progreso;

    public Horarios[] getJSONNew(final String urlWebService) {

        try {

            URL url = new URL(urlWebService);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            StringBuilder sb = new StringBuilder();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String json;
            //   Log.d("Msg","In Background");
            while ((json = bufferedReader.readLine()) != null) {
                sb.append(json + "\n");
                // Log.d("Msg",json+"\n");
            }
            //Log.d("Msg",sb.toString().trim());
            mjson = sb.toString();

        } catch (Exception e) {
            mjson="Error";

        }





        try {
            JSONObject jsonObject = new JSONObject(mjson);
         //   String estado = jsonObject.getString("estado");

            //JSONObject object = jsonObject.getJSONObject("metas");
            JSONArray mensaje = jsonObject.getJSONArray("records");
            // Parsear con Gson
            horarios = gson.fromJson(mensaje.toString(), Horarios[].class);
        }catch (JSONException e){

            Log.d("jSon Error:","El Error es" + e.toString());
        };

        return horarios;
    }

}
