package com.JSONs;

import android.util.Log;

import com.BaseDatosLocal.imagenAlumno;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by avarela6 on 23/1/2019.
 */

public class getJsonImagenAlumno {

    /**
     * Created by varelald on 03/02/2018.
     */

    public static String mjsonT;
    public static imagenAlumno[] imagenes;
    private static Gson gson = new Gson();
    ArrayList<imagenAlumno> listaImagenes = new ArrayList<imagenAlumno>();

    public imagenAlumno[] getJSONNew(final String urlWebService) {
        imagenes = null;
        try {

            URL url = new URL(urlWebService);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            StringBuilder sb = new StringBuilder();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String json;
            Log.d("Msg","In Background");
            while ((json = bufferedReader.readLine()) != null) {
                sb.append(json + "\n");
                Log.d("Msg",json+"\n");
            }
            Log.d("Msg",sb.toString().trim());
            mjsonT = sb.toString();

        } catch (Exception e) {
            mjsonT="Error";

        }

        try {
            JSONObject jsonObject = new JSONObject(mjsonT);
            JSONArray mensaje = jsonObject.getJSONArray("imagen");
            // Parsear con Gson

            for(int i = 0; i < mensaje.length(); i++) {
                JSONObject revista = mensaje.getJSONObject(i);

                // Creamos el objeto ImagenComercio
                imagenAlumno r = new imagenAlumno(revista.getString("dni"));
                r.setData(revista.getString("imagen"));

                // Almacenamos el objeto en el array que hemos creado anteriormente
                listaImagenes.add(r);
            }

            //      revistas = gson.fromJson(mensaje.toString(), Revista[].class);


            imagenAlumno[] imagenes1 = new imagenAlumno[listaImagenes.size()];

            imagenes1 = listaImagenes.toArray(imagenes1);
            imagenes = imagenes1;

        }catch (JSONException e){
            Log.d("jSon Error:","El Error es" + e.toString());
        };

        return imagenes;
    }


}


