package com.Tabs;


// basic model for this example

import android.os.Parcel;
import android.os.Parcelable;

public class DummyModel implements Parcelable {

    //private final String id;
    private final String title;
    private final String detail;
    private final String idInst;
    private final String idCom;
    private final String Tipo;

    public DummyModel(String id, String title, String detail,String idInst, String idCom,String tipo) {
      //  this.id = id;
        this.title = title;
        this.detail = detail;
        this.idInst = idInst;
        this.idCom = idCom;
        this.Tipo = tipo;
    }

    /*public String getId() {
        return id;
    }*/

    public String getTitle() {
        return title;
    }

    public String getDetail() {
        return detail;
    }

    public String getIdInst() {
        return idInst;
    }

    public String getIdCom() {
        return idCom;
    }

    public String getTipo() {return Tipo;}


    // parceleable

    protected DummyModel(Parcel in) {
        //id = in.readString();
        title = in.readString();
        detail = in.readString();
        idInst = in.readString();
        idCom = in.readString();
        Tipo = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        //dest.writeString(id);
        dest.writeString(title);
        dest.writeString(detail);
        dest.writeString(idInst);
        dest.writeString(idCom);
        dest.writeString(Tipo);
    }
    public static final Creator<DummyModel> CREATOR = new Creator<DummyModel>() {
        @Override
        public DummyModel createFromParcel(Parcel in) {
            return new DummyModel(in);
        }

        @Override
        public DummyModel[] newArray(int size) {
            return new DummyModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }


}
