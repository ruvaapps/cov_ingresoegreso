package com.Tabs;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;
import android.view.ViewGroup;

import com.covingresoegreso.MainActivity;
import com.covingresoegreso.R;

import java.util.List;
import java.util.Map;

/**
 * Created by gustavo.peiretti on 13/2/2017.
 */

public class PagerAdapter extends FragmentPagerAdapter {

    private int numTabs;
    //private DummyModel[] dummyModels0;
    //private DummyModel[] dummyModels1;
    private DummyModel[] dummyModels2,dummyModels1;
    public static String theCalendario = "N";
    public static String theExtra = "N";
    private ItemFragment ItemFragment0,ItemFragment1;
    private Map<Integer, String> mFragmentTags;
    private int container_id;
    private ViewGroup container;
    private List<Object> object;

    public PagerAdapter(FragmentManager fm, DummyModel[] dummyModels2) {

        // DummyModel[] dummyModels1, DummyModel[] dummyModels2
        super(fm);
        this.dummyModels2 = null;//MainActivity.lista;// dummyModels2;
        this.dummyModels1=null;
        this.numTabs = 2;
        //this.cc = cc;
    }

    @Override
    public Fragment getItem(int position) {

        Log.d("PageAdapter", "La posicion: " + position);
        ItemFragment tab = null;
/*
        if(position == 1){
            theCalendario = "Y";
            ItemFragment tab1 = ItemFragment.newInstance(dummyModels2, "Y");
            tab= tab1;
        }


        if(position ==0){
            theExtra = "N";
            ItemFragment tab2 = ItemFragment.newInstance(dummyModels1, "N");
            tab= tab2;
        }

        return tab;*/

switch(position){
    case 0:
        theCalendario = "N";
        return ItemFragment0.newInstance(dummyModels1, "Y");

    case 1:
        theCalendario = "Y";
        return ItemFragment1.newInstance(dummyModels2, "N");

    default:
            return null;

}

      /*  switch (position) {
            case 0:
                theCalendario = "N";
                ItemFragment tab1 = ItemFragment.newInstance(dummyModels1, "N");
                return tab1;
            case 1:
                theCalendario = "Y";
                ItemFragment tab2 = ItemFragment.newInstance(dummyModels2, "Y");
                return tab2;
            default:
                throw new RuntimeException("Tab position invalid " + position);
        }*/
    }

    @Override
    public int getCount() {
        return numTabs;
    }



    public String getFragmentTag(int pos){
        return "android:switcher:"+ R.id.pager+":"+pos;
    }

    public void NotifyDataChange(){
        this.notifyDataSetChanged();
    }

    public int getcontainerId(){
        return container_id;
    }

    public ViewGroup getContainer(){
        return this.container;
    }

    public List<Object> getObject(){
        return this.object;
    }

}
