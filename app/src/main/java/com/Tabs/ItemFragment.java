package com.Tabs;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.Calendario.CalendarCustomView;
import com.covingresoegreso.EventActivity;
import com.covingresoegreso.R;


/**
 * A fragment representing a dummyModels of DummyModel
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class ItemFragment extends Fragment {

    private static final String KEY_MODEL = "KEY_MODEL";

    private DummyModel[] dummyModels;
    private OnListFragmentInteractionListener interactionListener;
    private static String mCalendario;

    public ItemFragment() {
    }

    /**
     * Receive the model list
     */
    public static ItemFragment newInstance(DummyModel[] dummyModels,String Calendario) {
        ItemFragment fragment = new ItemFragment();



        if (Calendario == "N") {

            Bundle args = new Bundle();
            args.putParcelableArray(KEY_MODEL, dummyModels);
            fragment.setArguments(args);
        }


        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() == null) {
         //   throw new RuntimeException("You must to send a dummyModels ");
            Log.d("No manda Dummy","Sin Lista");
        }else {
            dummyModels = (DummyModel[]) getArguments().getParcelableArray(KEY_MODEL);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view ;

        if (PagerAdapter.theCalendario== "Y") {

            view = inflater.inflate(R.layout.activity_custom_calendar, container, false);
            PagerAdapter.theCalendario="N";
            // CalendarCustomView ccv = new CalendarCustomView(view.getContext());

        }
        else{
            view = inflater.inflate(R.layout.activity_event_layout, container, false);
            PagerAdapter.theCalendario="Y";

            //Context context = view.getContext();
            /*RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.setAdapter(new ItemRecyclerViewAdapter(dummyModels, interactionListener));*/
            //view = inflater.inflate(R.layout.event_layout, container, false);

        }
        //CalendarCustomView mView = (CalendarCustomView)findViewById(R.id.custom_calendar);
        return view;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // activity must implement OnListFragmentInteractionListener
        if (context instanceof OnListFragmentInteractionListener) {
            interactionListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        interactionListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * <p/>
     */
    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(DummyModel item);
    }
}
