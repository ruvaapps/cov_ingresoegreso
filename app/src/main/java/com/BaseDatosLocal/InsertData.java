package com.BaseDatosLocal;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * Created by varelald on 31/01/2018.
 */

public class InsertData {

    private static String mjson;

    public void InsertData(final String urlWebService){

        class SendPostReqAsyncTask extends AsyncTask<String, Void, String> {
            @Override
            protected String doInBackground(String... params) {



                /*List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

                nameValuePairs.add(new BasicNameValuePair("name", NameHolder));
                nameValuePairs.add(new BasicNameValuePair("email", EmailHolder));*/

                    try {

                        URL url = new URL(urlWebService);
                        HttpURLConnection con = (HttpURLConnection) url.openConnection();
                        StringBuilder sb = new StringBuilder();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));
                        String json;
                        //   Log.d("Msg","In Background");
                        while ((json = bufferedReader.readLine()) != null) {
                            sb.append(json + "\n");
                            // Log.d("Msg",json+"\n");
                        }
                        //Log.d("Msg",sb.toString().trim());
                        mjson = sb.toString();

                        return "Data Inserted Successfully";
                        //return sb.toString().trim();

                    } catch (Exception e) {
                        //mjson="Error";
                        Log.d("Error de Exception",e.getStackTrace().toString());
                        return e.getMessage();
                    }


            }

            @Override
            protected void onPostExecute(String result) {

                super.onPostExecute(result);

                //Toast.makeText(getApplicationContext(), "Data Submit Successfully", Toast.LENGTH_LONG).show();

            }
        }

        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();

        sendPostReqAsyncTask.execute(urlWebService);

    }
}
