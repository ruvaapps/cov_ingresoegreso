package com.BaseDatosLocal;

/**
 * Created by varelald on 03/01/2018.
 */

/**
 * Reflejo de la tabla 'meta' en la base de datos
 */
public class HorariosLocal {

    /*
    Atributos
     */
    private String horaInicio;
    private String minutoInicio;
    private String horaFin;
    private String minutoFin;
    private String periodoMinutos;


    public HorariosLocal(String horaInicio, String minutoInicio, String horaFin, String minutoFin, String periodoMinutos) {
        this.horaInicio = horaInicio;
        this.minutoInicio = minutoInicio;
        this.horaFin = horaFin;
        this.minutoFin = minutoFin;
        this.periodoMinutos = periodoMinutos;



    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public String getHoraFin() {
        return horaFin;
    }

    public String getPeriodoMinutos() {
        return periodoMinutos;
    }

    public String getMinutoInicio() {
        return minutoInicio;
    }

    public String getMinutoFin() {
        return minutoFin;
    }

}
