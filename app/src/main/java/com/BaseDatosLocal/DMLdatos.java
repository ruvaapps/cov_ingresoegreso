package com.BaseDatosLocal;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by varelald on 05/03/2018.
 */

public class DMLdatos {

    // inserts
    public void insertarComunicado(Context context,String idCom, String titulo, String descripcion, String fechaComunicado, String grado, String telefono,String idInst, String idAlum, String leido, String aceptado, String idComApp){

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(context,
                "administracion_es", null, 1);

        SQLiteDatabase bd = admin.getWritableDatabase();

        String MidCom = idCom;
        String Mtitulo = titulo;
        String Mdescripcion = descripcion;
        String MfechaComunicado = fechaComunicado;
        String Mgrado = grado;
        String Mtelefono = telefono;
        String MidInst = idInst;
        String MidAlum = idAlum;
        String Mleido = leido;
        String Maceptado = aceptado;
        String MidComApp = idComApp;

        ContentValues registro = new ContentValues();

        registro.put(" idCom",MidCom );
        registro.put(" titulo",Mtitulo );
        registro.put(" descripcion",Mdescripcion );
        registro.put(" fechaComunicado",MfechaComunicado );
        registro.put(" grado",Mgrado );
        registro.put(" telefono",Mtelefono );
        registro.put(" idInst",MidInst );
        registro.put(" idAlum",MidAlum );
        registro.put(" leido",Mleido );
        registro.put(" aceptado",Maceptado );
        registro.put(" idComApp",MidComApp );

        Cursor fila=null;
        //Valido que no exista en favoritos, para no insertar duplicados

        try {
            fila = bd.rawQuery("select * from comunicado where idCom=" + MidCom + " and idInst = '" + MidInst + "' and idAlum = '"+MidAlum+"'", null);
        } catch (Exception e){
            Log.d("Error insertando",e.getMessage());
        }

        if (fila.moveToFirst()) {

            //Toast.makeText(context, "Este comunicado ya esta cargado", Toast.LENGTH_SHORT).show();
            Log.d("Error insertando","Este comunicado ya esta cargado");

        } else {
            // los inserto en la base de datos
            bd.insert("comunicado", null, registro);

            bd.close();
        }


    }


    public void insertarInstitucion(Context context,String idInst, String nombre, String direccion, String telefono, String idFCM, String Url, String adjunto){

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(context,
                "administracion_es", null, 1);

        SQLiteDatabase bd = admin.getWritableDatabase();

        String MidInst = idInst;
        String Mnombre = nombre;
        String Mdireccion = direccion;
        String Mtelefono = telefono;
        String MidFCM = idFCM;
        String Murl = Url;
        String Madjunto = adjunto;
        //Integer Mdistancia = distancia;

        ContentValues registro = new ContentValues();

        registro.put(" idInst",MidInst );
        registro.put(" nombre",Mnombre );
        registro.put(" direccion",Mdireccion);
        registro.put(" telefono",Mtelefono );
        registro.put(" idFCM",MidFCM );
        registro.put(" URL",Murl);
        registro.put(" adjunto",Madjunto);

        //Valido que no exista en favoritos, para no insertar duplicados
        Cursor fila = bd.rawQuery( "select * from institucion where idInst='" + MidInst+"'", null);

        if (fila.moveToFirst()) {

            Toast.makeText(context, "Esta institucion ya esta registrada",
                    Toast.LENGTH_SHORT).show();


        } else {
            // los inserto en la base de datos
            bd.insert("institucion", null, registro);

            bd.close();
        }
    }


    public void insertarAlumno(Context context,String idAlum, String nombre, String apellido, String grado, String idInst, String foto){

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(context,
                "administracion_es", null, 1);

        SQLiteDatabase bd = admin.getWritableDatabase();

        String MidAlum = idAlum;
        String Mnombre = nombre;
        String Mapellido = apellido;
        String Mgrado = grado;
        String MidInst = idInst;
        String Mfoto = foto;

        //Integer Mdistancia = distancia;

        ContentValues registro = new ContentValues();

        registro.put(" idAlum",MidAlum );
        registro.put(" nombre",Mnombre );
        registro.put(" apellido",Mapellido);
        registro.put(" grado",Mgrado );
        registro.put(" idInst",MidInst );
        registro.put(" foto", Mfoto);



        //Valido que no exista en favoritos, para no insertar duplicados
        Cursor fila = bd.rawQuery( "select * from alumno where idAlum='" + MidAlum+"' and idInst='"+MidInst+"'", null);

        if (fila.moveToFirst()) {

            //Toast.makeText(context, "Esta alumno ya esta registrado", Toast.LENGTH_SHORT).show();


        } else {
            // los inserto en la base de datos
            bd.insert("alumno", null, registro);

            bd.close();
        }
    }

    public void insertarHorario(Context context,String horaInicio, String horaFin,String perdiodoMinutos, String toleranciaMinutos, String idGrado, String idInst){

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(context,
                "administracion_es", null, 1);

        SQLiteDatabase bd = admin.getWritableDatabase();

        String MhoraInicio = horaInicio;
        String MhoraFin = horaFin;
        String MperiodoMinutos = perdiodoMinutos;
        String MtoleranciaMinutos = toleranciaMinutos;
        String MidGrado = idGrado;
        String MidInst = idInst;

        //Integer Mdistancia = distancia;

        ContentValues registro = new ContentValues();

        registro.put(" horaInicio",MhoraInicio);
        registro.put(" horaFin",MhoraFin );
        registro.put(" periodoMinutos",MperiodoMinutos);

        registro.put(" toleranciaMinutos",MtoleranciaMinutos);
        registro.put(" idGrado",MidGrado);
        registro.put(" idInst",MidInst );



        //Valido que no exista en favoritos, para no insertar duplicados
        Cursor fila = bd.rawQuery( "select * from Horario where horaInicio='" + MhoraInicio+"' and horaFin='"+MhoraFin+"' and periodoMinutos='"+MperiodoMinutos+"' and toleranciaMinutos='"+MtoleranciaMinutos+"' and idGrado='"+MidGrado+"' and idInst='"+MidInst+"'", null);

        if (fila.moveToFirst()) {

            Log.d("Msg de DML","Esta Horario ya esta registrado");
            //Toast.makeText(context, "Esta Horario ya esta registrado", Toast.LENGTH_SHORT).show();


        } else {
            // los inserto en la base de datos
            bd.insert("Horario", null, registro);

            bd.close();
        }
    }


    public void insertarPagoCuota(Context context,String idInst, String idPago, String fechavencimiento, String fechapago, String datosPago, String idAlum, String monto){

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(context,
                "administracion_es", null, 1);

        SQLiteDatabase bd = admin.getWritableDatabase();

        /*String dni = et1.getText().toString();
        String nombre = et2.getText().toString();
        String ciudad = et3.getText().toString();
        String numero = et4.getText().toString();*/

        String MidInst = idInst;
        String MidPago = idPago;
        String Mfechavencimiento = fechavencimiento;
        String Mfechapago = fechapago;
        String MdatosPago = datosPago;
        String MidAlum = idAlum;
        String Mmonto = monto;

        ContentValues registro = new ContentValues();

        registro.put(" idInst",MidInst );
        registro.put(" idPago",MidPago);
        registro.put(" fechavencimiento",Mfechavencimiento);
        registro.put(" fechapago",Mfechapago );
        registro.put(" datosPago",MdatosPago );
        registro.put(" idAlum",MidAlum );
        registro.put(" monto",Mmonto );

        //Valido que no exista en favoritos, para no insertar duplicados
        Cursor fila = bd.rawQuery( "select * from pagos_cuotas where idInst='" + MidInst+"' and idPago = '" +MidPago+"'", null);

        if (fila.moveToFirst()) {

//            Toast.makeText(context, "Esta pago ya esta insertado", Toast.LENGTH_SHORT).show();


        } else {
            // los inserto en la base de datos
            bd.insert("pagos_cuotas", null, registro);

            bd.close();
        }
    }

    public void insertarEvento(Context context,String idEven, String hora, String mes, String fechaComunicado, String anio,String dia, String observacion,String imagen, String idInst, String idAlum, String leido){

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(context,
                "administracion_es", null, 1);

        SQLiteDatabase bd = admin.getWritableDatabase();

        String MidEven = idEven;
        String Mhora = hora;
        String Mmes = mes;
        String MfechaComunicado = fechaComunicado;
        String Manio = anio;
        String Mdia = dia;
        String Mobservacion = observacion;
        String Mimagen = imagen;
        String MidInst = idInst;
        String MidAlum = idAlum;
        String Mleido = leido;


        ContentValues registro = new ContentValues();

        registro.put(" idEven",MidEven );
        registro.put(" hora",Mhora );
        registro.put(" mes",Mmes );
        registro.put(" fechaComunicado",MfechaComunicado );
        registro.put(" anio",Manio );
        registro.put(" dia",Mdia );
        registro.put(" observacion",Mobservacion );
        registro.put(" imagen",Mimagen );
        registro.put(" idInst",MidInst );
        registro.put(" idAlum",MidAlum );
        registro.put(" leido",Mleido );

        Cursor fila=null;
        //Valido que no exista en favoritos, para no insertar duplicados

        try {
            fila = bd.rawQuery("select * from eventos where idEven=" + MidEven + " and idInst = '" + MidInst + "' and idAlum = '"+MidAlum+"'", null);
        } catch (Exception e){
            Log.d("Error insertando",e.getMessage());
        }

        if (fila.moveToFirst()) {

            //Toast.makeText(context, "Este comunicado ya esta cargado", Toast.LENGTH_SHORT).show();
            Log.d("Error insertando","Este evento ya esta cargado");

        } else {
            // los inserto en la base de datos
            bd.insert("eventos", null, registro);

            bd.close();
        }


    }

    public void insertarEntrada(Context context,String idAlumno, String idInst, String horaEntrada, String fechaEntrada){

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(context,
                "administracion_es", null, 1);

        SQLiteDatabase bd = admin.getWritableDatabase();

        String MidAlumno = idAlumno;
        String MidInst = idInst;
        String MhoraEntrada = horaEntrada;
        String MfechaEntrada = fechaEntrada;
        String Msincronizado = "N";



        ContentValues registro = new ContentValues();
        registro.put(" idAlumno",MidAlumno);
        registro.put(" idInst",MidInst);
        registro.put(" horaEntrada",MhoraEntrada);
        registro.put(" fechaEntrada",MfechaEntrada);
        registro.put(" Sincronizado",Msincronizado);

        Cursor fila=null;
        //Valido que no exista en favoritos, para no insertar duplicados

        try {
            fila = bd.rawQuery("select * from EntradaSalida where horaEntrada='" + MhoraEntrada + "' and fechaEntrada = '" + MfechaEntrada+ "' and idAlumno = '" + MidAlumno+ "' and idInst = '" + MidInst + "'", null);
        } catch (Exception e){
            Log.d("Error insertando",e.getMessage());
        }

        if (fila.moveToFirst()) {

            //Toast.makeText(context, "Este comunicado ya esta cargado", Toast.LENGTH_SHORT).show();
            Log.d("Error insertando","Este evento ya esta cargado");

        } else {
            // los inserto en la base de datos
            bd.insert("EntradaSalida", null, registro);

            bd.close();
        }


    }


    // updates


    public void actualizarSalida(Context context,String idAlumno, String idInst, String horaSalida, String fechaSalida){

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(context,
                "administracion_es", null, 1);

        SQLiteDatabase bd = admin.getWritableDatabase();


        String MidAlumno = idAlumno;
        String MidInst = idInst;
        String MhoraSalida = horaSalida;
        String MfechaSalida = fechaSalida;

        ContentValues registro = new ContentValues();

        registro.put(" horaSalida",MhoraSalida);
        registro.put(" fechaSalida",MfechaSalida );


//    registro.put(" idInst",MidInst );


        String select = "select * from EntradaSalida where idAlumno=" + MidAlumno +" and idInst = '"+ MidInst+"'";

        Log.d("Actualizar select:",select);

        //Valido que no exista en favoritos, para no insertar duplicados
        Cursor fila = bd.rawQuery( select, null);

        String mWhere = " idAlumno=" + MidAlumno+" and idInst = '"+ MidInst+"' and horaSalida is null";

        Log.d("ActualizarSalida Wehre",mWhere);

        Log.d("ActualizarSalida datos",MidAlumno+"-"+MidInst);

        if (fila.moveToFirst()) {

            int r=  bd.update("EntradaSalida", registro, mWhere, null);

            Log.d("despues del update"," Valor devuelto: "+r);

            bd.close();


        } else {

            Log.d("despues del update"," Este registro no existe");


        }
    }



    public void actualizarComunicado(Context context,String idCom, String titulo, String descripcion, String fechaComunicado, String grado, String telefono,String idInst, String idAlum, String leido, String aceptado){

    AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(context,
            "administracion_es", null, 1);

    SQLiteDatabase bd = admin.getWritableDatabase();


        String MidCom = idCom;
        String Mtitulo = titulo;
        String Mdescripcion = descripcion;
        String MfechaComunicado = fechaComunicado;
        String Mgrado = grado;
        String Mtelefono = telefono;
        String MidInst = idInst;
        String MidAlum = idAlum;
        String Mleido = leido;
        String Maceptado = aceptado;

    ContentValues registro = new ContentValues();

        registro.put(" titulo",Mtitulo );
        registro.put(" descripcion",Mdescripcion );
        registro.put(" fechaComunicado",MfechaComunicado );
        registro.put(" grado",Mgrado );
        registro.put(" telefono",Mtelefono );
        registro.put(" leido",Mleido );
        registro.put(" aceptado",Maceptado );


//    registro.put(" idInst",MidInst );


    //Valido que no exista en favoritos, para no insertar duplicados
    Cursor fila = bd.rawQuery( "select * from comunicado where idCom=" + MidCom +" and idInst = '"+ MidInst+"' and idAlum ='"+MidAlum+"'", null);

    String mWhere = " idCom=" + MidCom +" and idInst = '"+ MidInst+"' and idAlum ='"+MidAlum+"'";
    if (fila.moveToFirst()) {

       int r=  bd.update("comunicado", registro, mWhere, null);

        bd.close();


    } else {

        Toast.makeText(context, "Este comunicado NO esta cargado",
                Toast.LENGTH_SHORT).show();

    }
}

    public void actualizarComunicadoAceptado(Context context,String idCom, String idInst, String idAlum,String Aceptado){

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(context,
                "administracion_es", null, 1);

        SQLiteDatabase bd = admin.getWritableDatabase();


        String MidCom = idCom;
        String MidInst = idInst;
        String MidAlum = idAlum;
        String mAceptado = Aceptado;

        ContentValues registro = new ContentValues();

//    registro.put(" idCom",MidCom );
        registro.put(" aceptado",mAceptado );

//    registro.put(" idInst",MidInst );


        //Valido que no exista en favoritos, para no insertar duplicados
        Cursor fila = bd.rawQuery( "select * from comunicado where idCom=" + MidCom +" and idInst = '"+ MidInst+"' and idAlum = '"+MidAlum+"'", null);

        String mWhere = " idCom=" + MidCom +" and idInst = '"+ MidInst+"' and idAlum = '"+MidAlum+"'";
        if (fila.moveToFirst()) {

            int r=  bd.update("comunicado", registro, mWhere, null);

            bd.close();


        } else {

            Toast.makeText(context, "Este comunicado NO esta cargado",
                    Toast.LENGTH_SHORT).show();

        }
    }

    public void actualizarComunicadoLeido(Context context,String idCom, String idInst, String idAlum,String Leido){

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(context,
                "administracion_es", null, 1);

        SQLiteDatabase bd = admin.getWritableDatabase();


        String MidCom = idCom;
        String MidInst = idInst;
        String MidAlum = idAlum;
        String mLeido = Leido;

        ContentValues registro = new ContentValues();

//    registro.put(" idCom",MidCom );
        registro.put(" leido",mLeido );

//    registro.put(" idInst",MidInst );


        //Valido que no exista en favoritos, para no insertar duplicados
        Cursor fila = bd.rawQuery( "select * from comunicado where idCom=" + MidCom +" and idInst = '"+ MidInst+"' and idAlum = '"+MidAlum+"'", null);

        String mWhere = " idCom=" + MidCom +" and idInst = '"+ MidInst+"' and idAlum = '"+MidAlum+"'";
        if (fila.moveToFirst()) {

            int r=  bd.update("comunicado", registro, mWhere, null);

            bd.close();


        } else {

            Toast.makeText(context, "Este comunicado NO esta cargado",
                    Toast.LENGTH_SHORT).show();

        }
    }

    public void actualizarPagoCuota(Context context,String idInst, String idPago, String fechavencimiento, String fechapago, String datosPago, String idAlum, String monto){

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(context,
                "administracion_es", null, 1);

        SQLiteDatabase bd = admin.getWritableDatabase();


        String MidInst = idInst;
        String MidPago = idPago;
        String Mfechavencimiento = fechavencimiento;
        String Mfechapago = fechapago;
        String MdatosPago = datosPago;
        String MidAlum = idAlum;
        String Mmonto = monto;

        ContentValues registro = new ContentValues();

        registro.put(" idInst",MidInst );
        registro.put(" idPago",MidPago);
        registro.put(" fechavencimiento",Mfechavencimiento);
        registro.put(" fechapago",Mfechapago );
        registro.put(" datosPago",MdatosPago );
        registro.put(" idAlum",MidAlum );
        registro.put(" monto",Mmonto );

        //Valido que no exista en favoritos, para no insertar duplicados
        Cursor fila = bd.rawQuery( "select * from pagos_cuotas where idInst='" + MidInst+"' and idPago = '" +MidPago+"'", null);

        String mWhere = " idPago='" + MidPago +"' and idInst = '"+ MidInst+"' and idAlum ='"+MidAlum+"'";
        if (fila.moveToFirst()) {

//            Toast.makeText(context, "Esta pago ya esta insertado", Toast.LENGTH_SHORT).show();
// los inserto en la base de datos
            int r=  bd.update("pagos_cuotas", registro, mWhere, null);

            bd.close();

        } else {


            Toast.makeText(context, "Este comunicado NO esta cargado",
                    Toast.LENGTH_SHORT).show();

        }
    }


    public void actualizarEventoLeido(Context context,String idEven, String idInst, String idAlum,String Leido){

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(context,
                "administracion_es", null, 1);

        SQLiteDatabase bd = admin.getWritableDatabase();


        String MidEven = idEven;
        String MidInst = idInst;
        String MidAlum = idAlum;
        String mLeido = Leido;

        ContentValues registro = new ContentValues();

//    registro.put(" idCom",MidCom );
        registro.put(" leido",mLeido );

//    registro.put(" idInst",MidInst );


        //Valido que no exista en favoritos, para no insertar duplicados
        Cursor fila = bd.rawQuery( "select * from eventos where idEven=" + MidEven +" and idInst = '"+ MidInst+"' and idAlum = '"+MidAlum+"'", null);

        String mWhere = " idEven=" + MidEven +" and idInst = '"+ MidInst+"' and idAlum = '"+MidAlum+"'";
        if (fila.moveToFirst()) {

            int r=  bd.update("eventos", registro, mWhere, null);

            bd.close();


        } else {

            Toast.makeText(context, "Este evento NO esta cargado",
                    Toast.LENGTH_SHORT).show();

        }
    }

    public void actualizarEvento(Context context,String idEven, String hora, String mes, String fechaComunicado, String anio,String dia, String observacion,String imagen, String idInst, String idAlum, String leido){

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(context,
                "administracion_es", null, 1);

        SQLiteDatabase bd = admin.getWritableDatabase();


        String MidEven = idEven;
        String Mhora = hora;
        String Mmes = mes;
        String MfechaComunicado = fechaComunicado;
        String Manio = anio;
        String Mdia = dia;
        String Mobservacion = observacion;
        String Mimagen = imagen;
        String MidInst = idInst;
        String MidAlum = idAlum;
        String Mleido = leido;


        ContentValues registro = new ContentValues();

//    registro.put(" idCom",MidCom );
        registro.put(" idEven",MidEven );
        registro.put(" hora",Mhora );
        registro.put(" mes",Mmes );
        registro.put(" fechaComunicado",MfechaComunicado );
        registro.put(" anio",Manio );
        registro.put(" dia",Mdia );
        registro.put(" observacion",Mobservacion );
        registro.put(" imagen",Mimagen );
        registro.put(" idInst",MidInst );
        registro.put(" idAlum",MidAlum );
        registro.put(" leido",Mleido );

//    registro.put(" idInst",MidInst );


        //Valido que no exista en favoritos, para no insertar duplicados
        Cursor fila = bd.rawQuery( "select * from eventos where idEven=" + MidEven +" and idInst = '"+ MidInst+"' and idAlum = '"+MidAlum+"'", null);

        String mWhere = " idEven=" + MidEven +" and idInst = '"+ MidInst+"' and idAlum = '"+MidAlum+"'";
        if (fila.moveToFirst()) {

            int r=  bd.update("eventos", registro, mWhere, null);

            bd.close();


        } else {

            Toast.makeText(context, "Este evento NO esta cargado",
                    Toast.LENGTH_SHORT).show();

        }
    }

    public void actualizarPagoCuota(Context context,String idInst, String idPago, String fechavencimiento, String fechapago, String datosPago, String idAlum){

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(context,
                "administracion_es", null, 1);

        SQLiteDatabase bd = admin.getWritableDatabase();


        String MidInst = idInst;
        String MidPago = idPago;
        String Mfechavencimiento = fechavencimiento;
        String Mfechapago = fechapago;
        String MdatosPago = datosPago;
        //Integer Mdistancia = distancia;

        ContentValues registro = new ContentValues();

//        registro.put(" idInst",MidInst );
//        registro.put(" idPago",MidPago);
        registro.put(" fechavencimiento",Mfechavencimiento);
        registro.put(" fechapago",Mfechapago );
        registro.put(" datosPago",MdatosPago );


        //Valido que no exista en favoritos, para no insertar duplicados
        Cursor fila = bd.rawQuery( "select * from pagos_cuotas where idInst='" + MidInst+"' and idPago = '" +MidPago+"'", null);

        String mWhere = " idInst='" + MidInst+"' and idPago = '" +MidPago+"'";
        if (fila.moveToFirst()) {

            int r = bd.update("pagos_cuotas", registro,mWhere,null);

            bd.close();


        } else {


            Toast.makeText(context, "Esta pago NO esta insertado",
                    Toast.LENGTH_SHORT).show();

        }
    }

    public String buscaMontoPago(Context context,String idInst, String idPago){

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(context,
                "administracion_es", null, 1);

        SQLiteDatabase bd = admin.getWritableDatabase();


        String MidInst = idInst;
        String MidPago = idPago;
        String Mmonto = "0";
        ContentValues registro = new ContentValues();

        //Valido que no exista en favoritos, para no insertar duplicados
        Cursor fila = bd.rawQuery( "select monto from pagos_cuotas where idInst='" + MidInst+"' and idPago = '" +MidPago+"'", null);

        if (fila.moveToFirst()) {

            Mmonto = fila.getString(0);

            bd.close();
        }

        return Mmonto;
    }

    public void borarInstitucion(Context context,String idInst){

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(context,
                "administracion_es", null, 1);

        SQLiteDatabase bd = admin.getWritableDatabase();

        String MidInst = idInst;


            bd.delete("institucion"," idInst='"+MidInst+"'",null);

            bd.close();

    }


    public String getUrlByIdInstitucion(Context mcontext,String   MidInst){



        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(mcontext,
                "administracion_es", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase();
        String sqlQueryInst =  "select i.url from institucion i where i.idInst='"+MidInst+"'";
        Cursor filaInst = bd.rawQuery(sqlQueryInst, null);


        String mURL= null;

        int i = 0;
        if (filaInst.moveToFirst()) {
            do {
                mURL = filaInst.getString(0);
                i = i + 1;

            } while (filaInst   .moveToNext());
        } else{

            bd.close();

        }

        return mURL;
    }


    // deletes

    public void deleteInstituciones (Context context){

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(context,
                "administracion_es", null, 1);

        SQLiteDatabase bd = admin.getWritableDatabase();


            int r=  bd.delete("institucion", null, null);

            bd.close();

    }

    public void deleteAlumnos (Context context){

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(context,
                "administracion_es", null, 1);

        SQLiteDatabase bd = admin.getWritableDatabase();

            int r=  bd.delete("alumno", null, null);

            bd.close();



    }


// Controles de existencia


    public boolean validarSalida(Context context,String idAlumno, String idInst){

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(context,
                "administracion_es", null, 1);

        SQLiteDatabase bd = admin.getWritableDatabase();


        String MidAlumno = idAlumno;
        String MidInst = idInst;

        ContentValues registro = new ContentValues();


//    registro.put(" idInst",MidInst );


        String select = "select * from EntradaSalida where idAlumno=" + MidAlumno +" and idInst = '"+ MidInst+"' and horaSalida is null";

        Log.d("Actualizar select:",select);

        //Valido que no exista en favoritos, para no insertar duplicados
        Cursor fila = bd.rawQuery( select, null);


        if (fila.moveToFirst()) {


            Log.d("despues del update"," Valor devuelto: ");

            bd.close();
            return true;

        } else {

            Log.d("despues del update"," Este registro no existe");
            bd.close();
            return false;
        }
    }



}
