package com.BaseDatosLocal;

/**
 * Created by varelald on 03/01/2018.
 */

/**
 * Reflejo de la tabla 'meta' en la base de datos
 */
public class Alumno {

    /*
    Atributos
     */
    private String idAlum;
    private String nombre;
    private String apellido;
    private String grado;
    private String idInst;
    private String foto;

    public Alumno(String idAlum, String nombre, String apellido, String grado,String idInst, String foto) {
        this.idAlum = idAlum;
        this.nombre = nombre;
        this.apellido = apellido;
        this.grado = grado;
        this.idInst = idInst;
        this.foto = foto;



    }

    public String getIdAlum() {
        return idAlum;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public String getGrado() {
        return grado;
    }

    public String getIdInst() {
        return idInst;
    }

    public String getFoto() {
        return foto;
    }


}
