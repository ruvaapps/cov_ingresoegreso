package com.BaseDatosLocal;

/**
 * Created by varelald on 03/01/2018.
 */

/**
 * Reflejo de la tabla 'meta' en la base de datos
 */
public class PagosXInstitucion {

    /*
    Atributos
     */
    private String idInst;
    private String nombre;
    private String cantidad;


    public PagosXInstitucion(String idInst, String nombre, String cantidad) {
        this.idInst = idInst;
        this.nombre = nombre;
        this.cantidad = cantidad;

    }

    public String getIdInst() {
        return idInst;
    }

    public String getNombre() {
        return nombre;
    }

    public String getCantidad() {
        return cantidad;
    }

}
