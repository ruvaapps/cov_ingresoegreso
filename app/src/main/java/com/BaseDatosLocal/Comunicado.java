package com.BaseDatosLocal;

/**
 * Created by varelald on 03/01/2018.
 */

/**
 * Reflejo de la tabla 'meta' en la base de datos
 */
public class Comunicado {

    /*
    Atributos
     */
    private String idCom;
    private String titulo;
    private String descripcion;
    private String idInst;
    private String fechaComunicado;
    private String grado;
    private String telefono;
    private String idAlum;
    private String leido;
    private String aceptado;
    private String idComApp;


    public Comunicado(String idCom, String titulo, String descripcion, String fechaComunicado, String grado, String telefono, String idInst, String idAlum,String leido, String aceptado, String idComApp) {
        this.idCom = idCom;
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.idInst = idInst;
        this.fechaComunicado = fechaComunicado;
        this.grado = grado;
        this.telefono = telefono;
        this.idAlum = idAlum;
        this.leido = leido;
        this.aceptado=aceptado;
        this.idComApp = idComApp;

    }

    public String getIdCom() {
        return idCom;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getFechaComunicado() {
        return fechaComunicado;
    }

    public String getGrado() {
        return grado;
    }

    public  String getTelefono() {return telefono; }

    public String getidInst(){return  idInst;}

    public String getIdAlum(){return  idAlum;}

    public String getLeido(){return  leido;}

    public String getAceptado(){return  aceptado;}

    public String getIdComApp(){return  idComApp;}
}
