package com.BaseDatosLocal;

/**
 * Created by varelald on 26/02/2018.
 */

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class AdminSQLiteOpenHelper extends SQLiteOpenHelper {

    public AdminSQLiteOpenHelper(Context context, String nombre, CursorFactory factory, int version) {

        super(context, nombre, factory, version);

    }

    @Override

    public void onCreate(SQLiteDatabase db) {

        Log.d("queri busca all PAG","antes de crear la tablas");
        //aquí creamos la tabla de usuario (dni, nombre, ciudad, numero)
        //db.execSQL("create table meta(dni integer primary key, nombre text, ciudad text, numero integer)");
        db.execSQL("create table institucion(idInst text ,nombre text,direccion text,telefono text,idFcm text, URL text, adjunto text)");
        db.execSQL("create table alumno(idAlum text ,nombre text,apellido text,grado text,idInst text,foto text)");
        db.execSQL("create table eventos(idEven integer ,hora text,mes text,fechaComunicado text,anio text, dia text, observacion text,imagen text,idInst text,idAlum text,leido text)");
        db.execSQL("create table estructuraCalendario(idEst integer ,hora_inicio text,hora_fin text,fechaEstructura date,modulo text,tolerancia text,idInst text,idAlum text,leido text)");
        db.execSQL("create table Calendario(idCale integer ,materia text,hora text,fechaCalendario date,mes text,icono text,idInst text,idAlum text,leido text)");
        db.execSQL("create table Horario(horaInicio text ,horaFin text,periodoMinutos integer,toleranciaMinutos integer,idGrado integer,idInst integer)");
        db.execSQL("create table EntradaSalida(horaEntrada text ,horaSalida text,fechaEntrada text,fechaSalida text,idAlumno text, idInst text,Sincronizado text)");

        Log.d("queri busca all PAG","creo la tablas");

    }

    @Override

    public void onUpgrade(SQLiteDatabase db, int version1, int version2) {

        Log.d("queri busca all PAG","creo la tablas upgrade");
        db.execSQL("drop table if exists institucion");
        db.execSQL("create table institucion(idInst text ,nombre text,direccion text,telefono text,idFcm text, URL text, adjunto text)");


        db.execSQL("drop table if exists alumno");
        db.execSQL("create table alumno(idAlum text ,nombre text,apellido text,grado text,idInst text, foto text)");

        db.execSQL("drop table if exists eventos");
        db.execSQL("create table eventos(idEven integer ,hora text,mes text,fechaComunicado text,anio text,dia text, observacion text,imagen text ,idInst text,idAlum text,leido text)");

        db.execSQL("drop table if exists estructuraCalendario");
        db.execSQL("create table estructuraCalendario(idEst integer ,hora_inicio text,hora_fin text,fechaEstructura date,modulo text,tolerancia text,idInst text,idAlum text,leido text)");

        db.execSQL("drop table if exists Calendario");
        db.execSQL("create table Calendario(idCale integer ,materia text,hora text,fechaCalendario date,mes text,icono text,idInst text,idAlum text,leido text)");

        db.execSQL("drop table if exists Horario");
        db.execSQL("create table Horario(horaInicio text ,horaFin text,periodoMinutos integer,toleranciaMinutos integer,idGrado integer,idInst integer)");

        db.execSQL("drop table if exists EntradaSalida");
        db.execSQL("create table EntradaSalida(horaEntrada text ,horaSalida text,fechaEntrada text,fechaSalida text,idAlumno text, idInst text, Sincronizado text)");

    }


}