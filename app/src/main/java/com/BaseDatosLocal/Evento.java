package com.BaseDatosLocal;

/**
 * Created by varelald on 03/01/2018.
 */

/**
 * Reflejo de la tabla 'meta' en la base de datos
 */
public class Evento {

    /*
    Atributos
     */
    private String idEve;
    private String hora;
    private String mes;
    private String idInst;
    private String fechaComunicado;
    private String anio;
    private String dia;
    private String observacion;
    private String imagen;
    private String idAlum;
    private String leido;


    public Evento(String idEve, String hora, String mes, String fechaComunicado, String anio,String dia, String observacion,String imagen, String idInst, String idAlum, String leido) {
        this.idEve = idEve;
        this.hora = hora;
        this.mes = mes;
        this.idInst = idInst;
        this.fechaComunicado = fechaComunicado;
        this.anio = anio;
        this.dia= dia;
        this.observacion = observacion;
        this.imagen = imagen;
        this.idAlum = idAlum;
        this.leido = leido;


    }

    public String getIdEve() {
        return idEve;
    }

    public String getHora() {
        return hora;
    }

    public String getMes() {
        return mes;
    }

    public String getFechaComunicado() {
        return fechaComunicado;
    }

    public String getAnio() {
        return anio;
    }

    public String getDia() {
        return dia;
    }

    public  String getObservacion() {return observacion; }

    public  String getImagen() {return imagen; }

    public String getidInst(){return  idInst;}

    public String getIdAlum(){return  idAlum;}

    public String getLeido(){return  leido;}



}
