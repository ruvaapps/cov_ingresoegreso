package com.BaseDatosLocal;

/**
 * Created by varelald on 03/01/2018.
 */

/**
 * Reflejo de la tabla 'meta' en la base de datos
 */
public class Horarios {

    /*
    Atributos
     */
    private String horaInicio;
    private String horaFin;
    private String periodoMinutos;
    private String toleranciaMinutos;
    private String idGrado;
    private String idInst;


    public Horarios(String horaInicio, String horaFin, String periodoMinutos, String toleranciaMinutos, String idGrado, String idInst) {
        this.horaInicio = horaInicio;
        this.horaFin = horaFin;
        this.periodoMinutos = periodoMinutos;
        this.toleranciaMinutos = toleranciaMinutos;
        this.idGrado = idGrado;
        this.idInst = idInst;



    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public String getHoraFin() {
        return horaFin;
    }

    public String getPeriodoMinutos() {
        return periodoMinutos;
    }

    public String getToleranciaMinutos() {
        return toleranciaMinutos;
    }

    public String getIdGrado() {
        return idGrado;
    }

    public String getIdInst() {
        return idInst;
    }
}
