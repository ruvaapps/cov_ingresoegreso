package com.BaseDatosLocal;

/**
 * Created by varelald on 03/01/2018.
 */

/**
 * Reflejo de la tabla 'meta' en la base de datos
 */
public class Institucion {

    /*
    Atributos
     */
    private String idInst;
    private String nombre;
    private String direccion;
    private String telefono;
    private String idFCM;
    private String URL;
    private String adjunto;

    public Institucion(String idInst, String nombre, String direccion, String telefono, String idFCM, String URL, String adjunto ) {
        this.idInst = idInst;
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
        this.idFCM = idFCM;
        this.URL = URL;
        this.adjunto = adjunto;

    }

    public String getIdInst() {
        return idInst;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getIdFCM() {
        return idFCM;
    }

    public String getURL() {
        return URL;
    }

    public String getAdjunto() {
        return adjunto;
    }
}
