package com.BaseDatosLocal;

/**
 * Created by varelald on 03/01/2018.
 */

/**
 * Reflejo de la tabla 'meta' en la base de datos
 */
public class PagosCuotas {

    /*
    Atributos
     */
    private String idInst;
    private String idPago;
    private String fechavencimiento;
    private String fechapago;
    private String datosPago;
    private String idAlum;
    private String monto;


    public PagosCuotas(String idInst, String idPago, String fechavencimiento, String fechapago, String datosPago , String idAlum, String monto) {
        this.idInst = idInst;
        this.idPago = idPago;
        this.fechavencimiento = fechavencimiento;
        this.fechapago = fechapago;
        this.datosPago = datosPago;
        this.idAlum = idAlum;
        this.monto = monto;

    }

    public String getIdInst() {
        return idInst;
    }

    public String getIdPago() {
        return idPago;
    }

    public String getFechavencimiento() {
        return fechavencimiento;
    }

    public String getFechapago() {
        return fechapago;
    }

    public String getDatosPago() {
        return datosPago;
    }

    public String getIdAlum() {
        return idAlum;
    }


    public String getMonto() {
        return monto;
    }
}
