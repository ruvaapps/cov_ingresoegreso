package com.BaseDatosLocal;

/**
 * Created by varelald on 03/01/2018.
 */

/**
 * Reflejo de la tabla 'meta' en la base de datos
 */
public class ComunicadosXAlumno {

    /*
    Atributos
     */
    private String idInst;
    private String nombre;
    private String cantidad;
    private String idAlum;


    public ComunicadosXAlumno(String idInst, String nombre, String cantidad, String idAlum) {
        this.idInst = idInst;
        this.nombre = nombre;
        this.cantidad = cantidad;
        this.idAlum = idAlum;


    }

    public String getIdInst() {
        return idInst;
    }

    public String getNombre() {
        return nombre;
    }

    public String getCantidad() {
        return cantidad;
    }

    public String getIdAlum() {
        return idAlum;
    }
}
