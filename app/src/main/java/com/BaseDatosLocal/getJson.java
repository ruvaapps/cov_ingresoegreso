package com.BaseDatosLocal;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * Created by varelald on 09/01/2018.
 */

public class getJson {

    public static String mjson;
    public static Comunicado[] metas;
    private static Gson gson = new Gson();
    public static int Progreso;
    private ProgressDialog progreso;

    public Comunicado[] getJSON(final String urlWebService) {

        class GetJSON extends AsyncTask<Void, Void, String> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mjson=null;
            }


            @Override
            protected void onPostExecute(String s) {

                //Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
                //try {
                    //loadIntoListView(s);

                    Log.d("jSon on execute:",s.toString());

                /*} catch (JSONException e) {
                    e.printStackTrace();
                }*/

                super.onPostExecute(s);

            }

            @Override
            protected String doInBackground(Void... voids) {

               try {

                    URL url = new URL(urlWebService);
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    StringBuilder sb = new StringBuilder();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    String json;
                 //   Log.d("Msg","In Background");
                    while ((json = bufferedReader.readLine()) != null) {
                        sb.append(json + "\n");
                       // Log.d("Msg",json+"\n");
                    }
                    //Log.d("Msg",sb.toString().trim());
                    mjson = sb.toString();
                    return sb.toString().trim();
                } catch (Exception e) {
                    mjson="Error";
                    return e.getMessage();
                }
            }
        }

       GetJSON getJSON = new GetJSON();
       try {
           getJSON.execute();
       } catch (Exception e) {
           Log.d("GetJson Error:", "El Error es" + e.toString());
       }

        Progreso = 100;

       while  (mjson == null) {
           try {
               Log.d("Esperando","10 ms");
               Thread.sleep(10);
               Progreso -=5;
           } catch (InterruptedException e) {
               e.printStackTrace();
           }
       }

       Progreso = 0;

    try {
        JSONObject jsonObject = new JSONObject(mjson);
        String estado = jsonObject.getString("estado");

        //JSONObject object = jsonObject.getJSONObject("metas");
        JSONArray mensaje = jsonObject.getJSONArray("metas");
        // Parsear con Gson
        metas = gson.fromJson(mensaje.toString(), Comunicado[].class);
    }catch (JSONException e){
        Log.d("jSon Error:","El Error es" + e.toString());
    };

        return metas;
    }

}
