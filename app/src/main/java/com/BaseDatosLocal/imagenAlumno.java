package com.BaseDatosLocal;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

/**
 * Created by avarela6 on 23/1/2019.
 */

public class imagenAlumno {
    /*
  Atributos
   */
    private Bitmap Imagen;
    private String Data;
    private String dni;


    public imagenAlumno(String dni) {
        this.dni = dni;

    }



    public String getDni(){return dni;};

    public String getData() {
        return Data;
    }

    public void setData(String data) {
        this.Data = data;
        try {
            byte[] byteData = Base64.decode(data, Base64.DEFAULT);
            this.Imagen = BitmapFactory.decodeByteArray( byteData, 0,
                    byteData.length);
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    public Bitmap getLogo() {
        return Imagen;
    }

}
