package com.covingresoegreso;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.PowerManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.BaseDatosLocal.AdminSQLiteOpenHelper;
import com.BaseDatosLocal.DMLdatos;
import com.BaseDatosLocal.Evento;
import com.BaseDatosLocal.HorariosLocal;
import com.Calendario.CalendarCustomView;
import com.JSONs.getJsonImagenAlumno;
import com.Tabs.DummyModel;
import com.Tabs.ItemFragment;
import com.Tabs.PagerAdapter;
import com.Utilities.cargarAlumnosLocal;
import com.Utilities.cargarImagenLocal;
import com.Utilities.cargarHorarioLocal;
import com.covingresoegreso.Dummy.DummyContentSincronizacion;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.JSONs.getJsonImagenAlumno.imagenes;

public class MainActivity extends AppCompatActivity
        implements ItemFragment.OnListFragmentInteractionListener
{

    public static String mIdInst;
    public static Button mBtnEntrada;
    public static Button mBtnSalida;
    public static Button mBtnSincronizar;
    private static Button mBtnGrabar,mBtnCancelar;

    public static List<String> listaAlum = new ArrayList<String>();
    public static cargarAlumnosLocal CargarAlumnos;
    public static cargarImagenLocal CargarImagenes;
    public static Evento[] mEven;
    public static Spinner spinner_sin_registrar;
    private static AutoCompleteTextView textView;
    private static String idAlumnoSeleccionado;
    private static ImageView mImgAlumno;
    private static Bitmap imgAlum;
    private static HorariosLocal[] mHorarios;
    public static  DummyModel[] lista ;
    static ViewPager viewPager;
    private PagerAdapter adapter;

    public static int mHoraInicio,mMinutoInicio ,mMinutoPeriodo ,mHoraFin ,mMinutoFin;
    protected PowerManager.WakeLock wakelock;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final PowerManager pm=(PowerManager)getSystemService(Context.POWER_SERVICE);
        this.wakelock=pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "etiqueta");
        wakelock.acquire();


        mHoraInicio = 0;
        mMinutoInicio = 0;
        mMinutoPeriodo = 1;
        mHoraFin = 0;
        mMinutoFin = 0;


        mIdInst = ConfigurarAplicacion.getValuePreference(MainActivity.this,"Inst");
        try {
            setContentView(R.layout.activity_main);
        }catch (Exception e)
        {
            Log.d("Error activity main","ERROR: "+e.getMessage());
        }

        mImgAlumno = (ImageView) findViewById(R.id.imgAlumno);

        viewPager = (ViewPager) findViewById(R.id.pager);





        viewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override public boolean onTouch(View v, MotionEvent event) { return true; } }
        );




        mBtnEntrada = (Button)findViewById(R.id.btnEntrada);
        mBtnEntrada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Mi codigo*/
                Context context = v.getContext();
                Spinner mySpinner=(Spinner) findViewById(R.id.spAlumno);

                String mAlumno = mySpinner.getSelectedItem().toString();

                String idAlumno = mAlumno.substring(mAlumno.indexOf("-"),mAlumno.length());

                Log.d("id alumno seleccionado",idAlumnoSeleccionado);

                Calendar c = Calendar.getInstance();
                SimpleDateFormat Hora = new SimpleDateFormat("HH:mm:ss");
                SimpleDateFormat Fecha = new SimpleDateFormat("dd/MM/yyyy");

                String strHora = Hora.format(c.getTime());
                String strFecha = Fecha.format(c.getTime());




                try {
                    DMLdatos dmLdatos = new DMLdatos();
                    dmLdatos.insertarEntrada(context,idAlumnoSeleccionado,mIdInst,strHora,strFecha);

                } catch(Exception e){
                    Log.d("Error insertando",e.getMessage());
                }

                //CargarAlumnos task = new CargarAlumnos();
                //task.execute();



                Toast.makeText(MainActivity.this, "Este boton registrar la entrada para "+idAlumno+"- Fecha: "+strFecha+"- Hora: "+strHora, Toast.LENGTH_SHORT).show();


                habilitarEntradaSalida();

                return;
            }
        });



        mBtnSalida = (Button)findViewById(R.id.btnSalida);
        mBtnSalida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Mi codigo*/
                Context context = v.getContext();
                Spinner mySpinner=(Spinner) findViewById(R.id.spAlumno);
                String mAlumno = mySpinner.getSelectedItem().toString();

                String idAlumno = mAlumno.substring(mAlumno.indexOf("-"),mAlumno.length());

                Calendar c = Calendar.getInstance();

                SimpleDateFormat Hora = new SimpleDateFormat("HH:mm:ss");
                SimpleDateFormat Fecha = new SimpleDateFormat("dd/MM/yyyy");

                String strHora = Hora.format(c.getTime());
                String strFecha = Fecha.format(c.getTime());


                try {
                    DMLdatos dmLdatos = new DMLdatos();

                    dmLdatos.actualizarSalida(context,idAlumnoSeleccionado,mIdInst,strHora,strFecha);
                } catch(Exception e){
                    Log.d("Error insertando",e.getMessage());
                }

                //CargarAlumnos task = new CargarAlumnos();
                //task.execute();




                Toast.makeText(MainActivity.this, "Este boton regitrara la salida para "+idAlumno +" - " +strFecha+strHora, Toast.LENGTH_SHORT).show();



                habilitarEntradaSalida();

                return;
            }
        });



        mBtnSincronizar = (Button)findViewById(R.id.btnSincronizar);
        mBtnSincronizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Mi codigo*/
                Context context = v.getContext();
                Toast.makeText(MainActivity.this, "este boton Sincronizara", Toast.LENGTH_SHORT).show();




                return;
            }
        });

        spinner_sin_registrar = (Spinner) findViewById(R.id.spAlumno);
        spinner_sin_registrar.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id)
            {
/*                String Alumno =  adapterView.getSelectedItem().toString();
                String idAlumno = Alumno.substring(Alumno.indexOf("-")+2,Alumno.length());

                idAlumnoSeleccionado = idAlumno;


                cargaHorario cargarHorario = new cargaHorario();
                cargarHorario.execute();


                cargaDummyList cargaLista = new cargaDummyList();
                cargaLista.execute();


                cargaimagen cargaImagen = new cargaimagen();
                cargaImagen.execute();

                habilitarEntradaSalida();

                Toast.makeText(adapterView.getContext(),"se mostrara la imagen "+idAlumnoSeleccionado, Toast.LENGTH_SHORT).show();
*/

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {    }
        });



        spinner_sin_registrar.setVisibility(View.INVISIBLE);

        textView = (AutoCompleteTextView) findViewById(R.id.actAlumno);
        textView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    String selectedItem = (String) adapterView.getItemAtPosition(i);


                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(textView.getWindowToken(), 0);

                String Alumno = String.valueOf(textView.getText());
                String idAlumno = Alumno.substring(Alumno.indexOf("-")+2,Alumno.length());

                idAlumnoSeleccionado = idAlumno;

                cargaHorario cargarHorario = new cargaHorario();
                cargarHorario.execute();


                cargaDummyList cargaLista = new cargaDummyList();
                cargaLista.execute();



                cargaimagen cargaImagen = new cargaimagen();
                cargaImagen.execute();

                habilitarEntradaSalida();




                //Toast.makeText(MainActivity.this,"Se selecciono "+selectedItem, Toast.LENGTH_SHORT).show();

            }
        });

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View arg0) {

                textView.setText("");
                textView.showDropDown();

                //viewPager.setAdapter(null);

                /*String Alumno = String.valueOf(textView.getText());
                String idAlumno = Alumno.substring(Alumno.indexOf("-")+2,Alumno.length());

                idAlumnoSeleccionado = idAlumno;

                cargaHorario cargarHorario = new cargaHorario();
                cargarHorario.execute();


                cargaDummyList cargaLista = new cargaDummyList();
                cargaLista.execute();


                cargaimagen cargaImagen = new cargaimagen();
                cargaImagen.execute();

                habilitarEntradaSalida();


                Toast.makeText(MainActivity.this,"Se selecciono "+idAlumnoSeleccionado, Toast.LENGTH_SHORT).show();

*/

            }
        });



       // context = getApplicationContext();
        CargarInstituciones task = new CargarInstituciones();
        task.execute();



    }

    public void showSoftKeyboard(View view){
        if(view.requestFocus()){
            InputMethodManager imm =(InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.menu_principal, menu);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        // Toast.makeText(this, "ID Menu: " +item.getTitle(), Toast.LENGTH_SHORT).show();
        if (item.getTitle().equals("Logo")){

            Intent intent = new Intent(this, MainActivity.class);

            try {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                this.startActivity(intent);
            } catch (Exception e){
                Log.d("Error start activity",e.getMessage());
            }

            this.finish();
            return true;

        }


        if (item.getTitle().equals("Conectar")){

            //llama a la funcion quesincroniza con todos los datos desde el server

            String sala = ConfigurarAplicacion.getValuePreference(MainActivity.this,"SALA");




            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

            builder.setMessage("¿Que Sincronizacion Desea?")
                    .setTitle("Advertencia")
                    .setCancelable(false)
                    .setNegativeButton("Cancelar",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    dialog.cancel();
                                }
                            })
                    .setPositiveButton("Copleto",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //SyncTotal = "Y";
                                    SincroInfo sync = new SincroInfo();
                                    sync.execute();
                                }
                            })
                    .setNeutralButton("Pendientes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //SyncTotal = "N";
                                    SincroInfo sync = new SincroInfo();
                                    sync.execute();
                                }
                            });
            AlertDialog alert = builder.create();
            alert.show();

            return true;

        }



        return super.onOptionsItemSelected(item);
    }


    public void cargarAdaptador () {
        //  ViewPager need a PagerAdapter
        //PagerAdapter
        adapter = new PagerAdapter(getSupportFragmentManager(), lista);
        viewPager.setAdapter(adapter);

        // Listeners
        //viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        //tabLayout.addOnTabSelectedListener(getOnTabSelectedListener(viewPager));



    }


    public void listarCalendario(Context context){



        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(context,
                "administracion_es", null, 1);

        SQLiteDatabase bd = admin.getWritableDatabase();


        String sqlQuery;


        sqlQuery = "select * from eventos where idAlum = '" + "idAlumnoSeleccionado"+"'";
        Cursor fila=null;
try {
    fila = bd.rawQuery(sqlQuery, null);
}catch(Exception e){
   Log.d("error DataBase : ",e.getMessage());
}
        mEven = new Evento[fila.getCount()];
        int i = 0;


        if (fila.moveToFirst()) {
            do {
                mEven[i] = new Evento(fila.getString(0),
                        fila.getString(1),
                        fila.getString(2),
                        fila.getString(3),
                        fila.getString(4),
                        fila.getString(5),
                        fila.getString(6),
                        fila.getString(7),
                        fila.getString(8),
                        fila.getString(9),
                        fila.getString(10));


                i = i + 1;
            } while (fila.moveToNext());
        } else{

            bd.close();

        }

        //Log.d("Mensaje","Datos del JSON" + mComu.toString());




    }

    @Override
    public void onListFragmentInteraction(DummyModel item) {

        Toast.makeText(MainActivity.this, "hizo click en el calendario", Toast.LENGTH_LONG).show();

        //Toast.makeText(TabExampleMainActivity.this, DummyModel.class.getSimpleName() + ":" + model.getId() + " - "  +model.getTitle(), Toast.LENGTH_LONG).show();

    }



    class SincroInfo extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {

            // aca se mostrara el mensaje de sincronizando
            Toast.makeText(MainActivity.this, "Comienza la sincronizacion", Toast.LENGTH_SHORT).show();
            super.onPreExecute();
        }


        @Override
        protected void onPostExecute(String s) {


            //Aca se terminara la sincro
            Toast.makeText(MainActivity.this, "Finalizo la Sincronizacion", Toast.LENGTH_SHORT).show();
            super.onPostExecute(s);

        }
        @Override
        protected String doInBackground(Void... voids) {

            DummyContentSincronizacion.setContext(getApplicationContext());
            return "Ok";
        }
    }


    class CargarInstituciones extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            //            alertUbicacion.show();
            //pb.setVisibility(View.VISIBLE);
            //mBtnConsLocal.setEnabled(false);
            //mBtnConsRevista.setEnabled(false);
            super.onPreExecute();
        }


        @Override
        protected void onPostExecute(String s) {
            //          alertUbicacion.hide();
            //pb.setVisibility(View.INVISIBLE);
            //mBtnConsLocal.setEnabled(true);
            //mBtnConsRevista.setEnabled(true);

            //Log.d("Mensaje ","Cargando la lista "+listaInst.size());



            listaAlum = new ArrayList<String>(cargarAlumnosLocal.valuesAlumns);

        Log.d("Carga Alumnos: ",cargarAlumnosLocal.valuesAlumns.toString());
        Log.d("Carga Alumnos: ",listaAlum.toString());

                ArrayAdapter<String> spinner_adapter_sreg = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_spinner_item, listaAlum);
                spinner_adapter_sreg.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner_sin_registrar.setAdapter(spinner_adapter_sreg);


            final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_spinner_item, listaAlum);
            spinner_adapter_sreg.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            textView.setAdapter(arrayAdapter);



            Log.d("RegInst","Cargo la Lista");

            super.onPostExecute(s);

        }
        @SuppressLint("MissingPermission")
        @Override
        protected String doInBackground(Void... voids) {


           Context context = getApplicationContext();

            CargarAlumnos.cargarAlumnsLocal("","http://www.revistabarrial.com.ar/cuadernoComunicado/FolderSalas",context);

            Log.d("Carga Alumnos Do: ",cargarAlumnosLocal.valuesAlumns.toString());



            return "Ok";
        }
    }


    class cargaDummyList extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }


        @Override
        protected void onPostExecute(String s) {
            Log.d("jSon on execute:",s.toString());
            lista = createDummyListModel("Calendario");

            Log.d("Lista cargada","-" + lista.length);

            cargarAdaptador();
            super.onPostExecute(s);

        }
        @Override
        protected String doInBackground(Void... voids) {


            listarCalendario(MainActivity.this);
            return "Ok";
        }
    }

    private DummyModel[] createDummyListModel(String msj) {
        List<DummyModel> l = new ArrayList<>();

        return l.toArray(new DummyModel[l.size()]);
    }



    class cargaimagen extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            imgAlum = null;
           // mImgAlumno.setImageBitmap(imgAlum);
            super.onPreExecute();
        }


        @Override
        protected void onPostExecute(String s) {
            Log.d("jSon on execute:",s.toString());

            //mImgAlumno.setImageBitmap(imgAlum);

            //

            mImgAlumno.setImageBitmap(imgAlum);
            super.onPostExecute(s);

        }
        @Override
        protected String doInBackground(Void... voids) {
            imgAlum= null;

            Context context = getApplicationContext();

            Log.d("Alumno Seleccionado",idAlumnoSeleccionado);
            cargarImagenLocal.cargarImagenLocal(idAlumnoSeleccionado,context);

            imgAlum = cargarImagenLocal.valuesImagenes.get(0);

            Log.d("Imagen Despues", String.valueOf(imgAlum));
            return "Ok";
        }
    }


    class cargaHorario extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }


        @Override
        protected void onPostExecute(String s) {
            Log.d("jSon on execute:",s.toString());


            mHoraInicio = Integer.parseInt(mHorarios[0].getHoraInicio());
            mMinutoInicio = Integer.parseInt(mHorarios[0].getMinutoInicio());
            mHoraFin = Integer.parseInt(mHorarios[0].getHoraFin());
            mMinutoFin = Integer.parseInt(mHorarios[0].getMinutoFin());
            mMinutoPeriodo = Integer.parseInt(mHorarios[0].getPeriodoMinutos());

            Log.d("Datos Horarios","-"+mHoraInicio+"-"+mHoraInicio+"-"+mMinutoInicio+"-"+mMinutoPeriodo);



            super.onPostExecute(s);

        }
        @Override
        protected String doInBackground(Void... voids) {

            mHorarios = null;

            Context context = getApplicationContext();

            cargarHorarioLocal.cargarHorarioLocal(idAlumnoSeleccionado,context);

            mHorarios = cargarHorarioLocal.mHorario;

            Log.d("HorariosLocales", String.valueOf(mHorarios.length));
            return "Ok";
        }
    }

private void habilitarEntradaSalida()
{

    Boolean b=false;
    try{

           DMLdatos dmLdatos = new DMLdatos();

            b = dmLdatos.validarSalida(getApplicationContext(),idAlumnoSeleccionado,mIdInst);



    }catch( Exception e){
        Log.d("Error:",e.getMessage());
    }


    if(b){
        mBtnSalida.setEnabled(true);
        mBtnEntrada.setEnabled(false);
    }else{
        mBtnSalida.setEnabled(false);
        mBtnEntrada.setEnabled(true);
    }


}

public static void setearPager(int index){
    viewPager.setCurrentItem(index);
}

    protected void onResume(){
        super.onResume();
        wakelock.acquire();
    }
    public void onSaveInstanceState(Bundle icicle) {
        super.onSaveInstanceState(icicle);
        this.wakelock.release();
    }


    }
