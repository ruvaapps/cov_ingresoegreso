package com.covingresoegreso.Dummy;

import android.content.Context;
import android.util.Log;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.BaseDatosLocal.Alumno;
import com.BaseDatosLocal.Comunicado;
import com.BaseDatosLocal.ComunicadosXAlumno;
import com.BaseDatosLocal.DMLdatos;
import com.BaseDatosLocal.Evento;
import com.BaseDatosLocal.EventosXAlumno;
import com.BaseDatosLocal.Horarios;
import com.BaseDatosLocal.PagosCuotas;
import com.BaseDatosLocal.PagosXAlumno;
import com.JSONs.getJsonAlumnos;
import com.JSONs.getJsonEvento;
import com.JSONs.getJsonHorarios;
import com.covingresoegreso.ConfigurarAplicacion;
import com.covingresoegreso.MainActivity;


/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class DummyContentSincronizacion {

    /**
     * An array of sample (dummy) items.
     */
    public static List<Alumno> ITEMSALUM = new ArrayList<Alumno>();
    public static List<Horarios> ITEMS = new ArrayList<Horarios>();
    public static List<EventosXAlumno> ITEMSEVENTOS = new ArrayList<EventosXAlumno>();

    public static Alumno[] mAlum;
    public static Horarios[] mHorario;
    public static Evento[] mEvento;
    public static Alumno[] alum;
    public static Horarios[] horarios;
    public static String mLista;

    public static String MidInst;
    private static int COUNTa;
    private static int COUNTh;
    private static int COUNTe;
    private static Context mcontext;
    /**
     * A map of sample (dummy) items, by ID.
     */
    public static final Map<String, DummyItem> ITEM_MAP = new HashMap<String, DummyItem>();

    private static DMLdatos dmLdatos= new DMLdatos();

    public static void setContext (Context context){

        mcontext = context;

        MidInst = MainActivity.mIdInst;
            listarComuicadosSync();

    }

    private static String mWhere;

    private static String urlQuery;

    private static getJsonEvento mjSONE;
    private static getJsonHorarios mjSONH;
    private static getJsonAlumnos mjSONA;

    /**
     * A dummy item representing a piece of content.
     */
    public static class DummyItem {
        public static String id;
        public static String idCom;
        public static String titulo;
        public static String descripcion;
        public static String fechaComunicado;
        public static String grado;
        public static String telefono;
        public static String idInst;

        public DummyItem(String id, String idCom, String titulo, String descripcion, String fechaComunicado, String grado, String telefono, String idInst) {

            this.id=id;
            this.idCom=idCom;
            this.titulo=titulo;
            this.descripcion=descripcion;
            this.fechaComunicado=fechaComunicado;
            this.grado=grado;
            this.telefono=telefono;
            this.idInst=idInst;
        }

        @Override
        public String toString() {
            return idCom+idInst;
        }
    }



    public static void listarComuicadosSync () {
        // Add some sample items.




        String mURL = dmLdatos.getUrlByIdInstitucion(mcontext,MidInst);
        mWhere = null;



        mWhere = "?inst="+MidInst+"&sala=" + ConfigurarAplicacion.getValuePreference(mcontext,"SALA");



            // busca los comunicados para sincronizarlos
                urlQuery = mURL+"/cuadernoComunicado/FolderSalas/php/obtener_calendarios.php" + mWhere;
                mjSONH = new getJsonHorarios();
                mHorario = mjSONH.getJSONNew(urlQuery);

                Log.d("queri busca all COM",urlQuery);

            // busca los pagos para sincronizarlos
                urlQuery = mURL+"/cuadernoComunicado/FolderSalas/php/obtener_alumnos.php" + mWhere;
                mjSONA = new getJsonAlumnos();
                mAlum = mjSONA.getJSONNew(urlQuery);


            Log.d("queri busca all PAG",urlQuery);



            //metas = mjSON.getJSONNew(+ "RevistaBarrial/obtener_metas_more.php" + mWhere);







       // Log.d("Mensaje","Datos del JSON" + mComu.toString());

        // grabo los comunicados que se deben sincronizar
        ITEMS.clear();

        if (mHorario != null) {
            COUNTh = mHorario.length;
            for (int i = 0; i <= COUNTh - 1; i++) {

                dmLdatos.insertarHorario(mcontext, mHorario[i].getHoraInicio(), mHorario[i].getHoraFin(),mHorario[i].getPeriodoMinutos(), mHorario[i].getToleranciaMinutos(), mHorario[i].getIdGrado(), mHorario[i].getIdInst());
                //addItem(createDummyItem(i,mComu[i]));
            }
        }


        // grabo los pagos que se deben sincronizar
        ITEMS.clear();

        if(mAlum != null) {
            COUNTa = mAlum.length;
            for (int i = 0; i <= COUNTa - 1; i++) {

                dmLdatos.insertarAlumno(mcontext, mAlum[i].getIdAlum(), mAlum[i].getNombre(), mAlum[i].getApellido(), mAlum[i].getGrado(),mAlum[i].getIdInst(), mAlum[i].getFoto());
                //addItem(createDummyItem(i,mComu[i]));
            }
        }


    }



}
