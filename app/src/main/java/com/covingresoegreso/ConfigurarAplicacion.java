package com.covingresoegreso;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.BaseDatosLocal.DMLdatos;

public class ConfigurarAplicacion extends AppCompatActivity {

    private static Button MbtnGrabar;
    private static Spinner MspSalas;
    private static EditText MtxtPass;
    private static String PREFS_KEY = "datoscovingresoegreso";
    private static  String Msala;

    private static DMLdatos dmLdatos= new DMLdatos();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configurar_aplicacion);
       // Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        MbtnGrabar = (Button) findViewById(R.id.btnGrabar);
        MspSalas = (Spinner) findViewById(R.id.spSala);
        MtxtPass = (EditText) findViewById(R.id.txtPass);


        MtxtPass.setVisibility(View.INVISIBLE);

            Msala = getValuePreference(ConfigurarAplicacion.this, "SALA");



      /*  if (!Msala.isEmpty()){
            Context context = ConfigurarAplicacion.this;

            Intent intent = new Intent(context, MainActivity.class);
            //intent.putExtra("idInst", mListaItems);
            context.startActivity(intent);
        }
*/

        MbtnGrabar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Mi codigo*/
                Context context = v.getContext();
               // Spinner mySpinner=(Spinner) findViewById(R.id.spAlumno);

                //String mAlumno = mySpinner.getSelectedItem().toString();

                //String idAlumno = mAlumno.substring(mAlumno.indexOf("-"),mAlumno.length());

                String ps = String.valueOf(MtxtPass.getText());
                if (ps.equals("aldo")){

                    dmLdatos.insertarInstitucion(ConfigurarAplicacion.this,"StaMa","Santa Maria","Camino San Carlos", "1111","nn","http://revistabarrial.com.ar","N");

                    saveValuePreference(ConfigurarAplicacion.this,"SALA","1");
                    saveValuePreference(ConfigurarAplicacion.this,"Inst","StaMa");
                    MtxtPass.setVisibility(View.INVISIBLE);
                    Context contextm = getApplication().getApplicationContext();
                    Intent intent = new Intent(context, MainActivity.class);
                    //intent.putExtra("MiLista", mListaItems);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    contextm.startActivity(intent);

                }else{

                   MtxtPass.setVisibility(View.VISIBLE);
                    Toast.makeText(ConfigurarAplicacion.this, "Ingrese la password correcta "+MtxtPass.getText(), Toast.LENGTH_SHORT).show();
                    return;
                }

                try {
//                    DMLdatos dmLdatos = new DMLdatos();

                } catch(Exception e){
                    Log.d("Error insertando",e.getMessage());
                }

                //CargarAlumnos task = new CargarAlumnos();
                //task.execute();

                Toast.makeText(ConfigurarAplicacion.this, "Este boton registra la sala", Toast.LENGTH_SHORT).show();


                return;
            }
        });



    }

    public static void saveValuePreference(Context context,String key, String value) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_KEY, MODE_PRIVATE);
        SharedPreferences.Editor editor;
        editor = settings.edit();
        editor.putString(key, value);
        editor.apply();
    }


    public static String  getValuePreference(Context context,String key) {
        SharedPreferences preferences = context.getSharedPreferences(PREFS_KEY, MODE_PRIVATE);
        return  preferences.getString(key, "");
    }


}

