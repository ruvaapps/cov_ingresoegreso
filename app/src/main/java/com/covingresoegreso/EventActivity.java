package com.covingresoegreso;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by avarela6 on 2/4/2019.
 */

public class EventActivity extends LinearLayout {
    private ImageView mBtnGrabar;
    private ImageView mBtnCancela;
    private Button mBtnPrueba;
    private Context context;




    public EventActivity(Context context) {
        super(context);

    }
    public EventActivity(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initializeUILayout();
        setmBtnGrabarClickEvent();
        setmBtnCancelaClickEvent();
    }
    public EventActivity(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }





    private void initializeUILayout(){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.event_layout, this);
        mBtnGrabar = (ImageView) view.findViewById(R.id.btnGrabarEvento);
        mBtnCancela = (ImageView) view.findViewById(R.id.btnCancelarEvento);


    }






    private void setmBtnGrabarClickEvent(){
        mBtnGrabar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(context, "Se vuelve al calendario grabar ", Toast.LENGTH_LONG).show();
                MainActivity.setearPager(0);


            }
        });
    }

    private void setmBtnCancelaClickEvent(){
        mBtnCancela.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(context, "Se vuelve al calendario cancelar", Toast.LENGTH_LONG).show();
                MainActivity.setearPager(0);


            }
        });
    }



}
